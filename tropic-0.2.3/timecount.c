/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>
#include <string.h>

/*-----------------------------------------------*/

#if (__AVR_ARCH__ > 1)
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#endif

/*-----------------------------------------------*/

#define __FILE_TIMECOUNT_C__

#include "globaltr.h"
#include "bhead.h"
#include "pinout.h"
#include "util.h"
#include "timecount.h"
#include "uarttx.h"

/*-----------------------------------------------*/

#if __TIME_GLOB__==1
uint32_t mssec;
#endif

/*-----------------------------------------------*/

volatile uint8_t wait8;

/*-----------------------------------------------*/

#if __TIME_COOUNT__==1 && __TIME_GLOB__==1

union uuu savesp;
uint32_t tick2;
uint16_t tim16;
uint32_t tim32a;
uint16_t tirquart;
uint8_t  tirqadc;
uint8_t  savespl;
uint8_t  savesph;

/*-----------------------------------------------*/

void showalltime( void ){
  
  uint32_t tmp;

  getbinstr( tim16, 5 );
  getbinstr( mssec, 7 );
  cli();
  tmp = tick2;
  sei();
  getbinstr( tirqadc, 5 );
  
  cli();
  tmp = savesp.n16;
  sei();
  getbinstr( tmp, 5 );
  RSout('\n');
}

#endif

/*-----------------------------------------------*/
/*
  185 us wait8
*/
ISR(TIMER2_OVF_vect) {

#if __TIME_COOUNT__==1

  union union_16_8 savesptmp;
  
  ++tick2;
  savesptmp.n8[0] = SPL;
  savesptmp.n8[1] = SPH;
  if( savesptmp.n16 < savesp.n16 )
    savesp.n16 = savesptmp.n16;

#endif

#if __RTSCTS==1 && UARTTXBUF==0
  if(( flag & XOFFSEND ) && _IS_CTS_CLR() ){
    _EN_UART_EMPTY();
  }
#endif

#if __RTSCTS==1 && UARTTXBUF==1
  if(( flag & XOFFSEND ) || bufuarthasdata() )
    if( _IS_CTS_CLR() ) _EN_UART_EMPTY();
#endif 

  if( wait8 ) --wait8;
}

/*-----------------------------------------------*/

#if __TIME_COOUNT__==1

static union union_32_8 timm;

uint32_t tim(void){
  uint32_t timn;
  timn = timm.n32;
  _REG_TCCR2 = 0;
  timm.n8[0] = TCNT2;
  timm.n8[1] = tick2;
  timm.n8[2] = tick2>>8;
  timm.n8[3] = tick2>>16;
  _REG_TCCR2 = (1<<CS21);     /* 8 presc */
  timn += 12;                 /* time this fun */
  return (timm.n32-timn)<<3; /* 8 presc */
}

/*-----------------------------------------------*/
/*
uint32_t timget(void){
  union union_32_8 tim32;
  _REG_TCCR2 = 0;
  tim32.n8[0] = TCNT2;
  tim32.n8[1] = tick2;
  tim32.n8[2] = tick2>>8;
  tim32.n8[3] = tick2>>16;
  _REG_TCCR2 = (1<<CS21);
  tim32.n32 += 11;
  return tim32.n32<<3;
}
*/
#endif

/*-----------------------------------------------*/

#if __SHOW_TIME==1 && __TIME_GLOB__==1

uint16_t timgetms(void){
  uint16_t timems;
  cli();
  timems = mssec;
  sei();
  return timems;
}
#else
uint16_t timgetms( void ){ return 0; }
#endif

/*-----------------------------------------------*/

void delay50us( void ){
  _delay_us(50);
}

/*-----------------------------------------------*/

void delay_x1ms(uint8_t delay){
  while(delay--) _delay_ms(1);
}

/*-----------------------------------------------*/

