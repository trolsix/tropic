/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>

#include "globaltr.h"
#include "bhead.h"
#include "pinout.h"
#include "uarttx.h"
#include "timecount.h"
#include "hardtrans.h"
#include "pic18f1220.h"
#include "readincoming.h"

/* ------------------------------------ */
/* only work with parity data */

void initvaluehex( uint16_t memsiz ){
  numdat = 0;
  prdat.progstadr16 = 0;
  prdat.progsiz = memsiz;
}

/* ------------------------------------ */

#if __GR_18f4220==1

/* ------------------------------------ */
/*     adres in hex file                */

#define ADR_FLASH  0x000000UL
#define ADR_USERID 0x200000UL
#define ADR_CONFIG 0x300000UL
#define ADR_EEPROM 0xF00000UL
#define ADR_REVID  0x3FFFFEUL

/* ------------------------------------ */

static void send16z8( uint16_t d ){
  send4bit( 0 );
  send8bitl( d );
  send8bitl( d>>8 );
}

/* ------------------------------------ */

static void sendzer( void ){
  send8bitl( 0 );
  send8bitl( 0 );
}

/* ------------------------------------ */

static void loadadr( uint32_t adr ){
  
  send4bit( 0 );
  send8bitl( adr>>16 );
  send8bitl( 0x0E );
  
  send16z8( 0x6EF8 );
  
  send4bit( 0 );
  send8bitl( adr>>8 );
  send8bitl( 0x0E );
  
  send16z8( 0x6EF7 );
  
  send4bit( 0 );
  send8bitl( adr );
  send8bitl( 0x0E );
  
  send16z8( 0x6EF6 );
}

/* ------------------------------------ */

static uint8_t readflash18f1220one( void ){
  send4bit( 0x09 );
  send8bitl( 0x00 );
  return read8bitl();
}
  
static void readflash18f1220( uint16_t memsiz ){
 
  initvaluehex( memsiz );

  while(1){
    if( 0 == memsiz-- ) break;
    wordtosave.n8[0] = readflash18f1220one();
    if( 0 == memsiz-- ) break;
    wordtosave.n8[1] = readflash18f1220one();
    rssend16univ( wordtosave.n16 );
  }
}

/* ------------------------------------ */

static void read18f1220( uint16_t memsiz ){
  loadadr( 0x000000 );
//  sendhighadr( 0x0000 );
  prdat.progstadr16 = 0;
  readflash18f1220( memsiz );
}

/* ------------------------------------ */
/* works only for parity memsiz */

static void readdata18f1220( uint16_t memsiz ){
  
  uint8_t adr = 0;
  
  /* need for out hex */
  sendhighadr( 0x00F0 );
  initvaluehex( memsiz );

  loadadr( 0x000000 );
  
  while(memsiz--){
    send16z8( 0x9EA6 );
    send16z8( 0x9CA6 );
    send4bit( 0 );
    send8bitl( adr );
    send8bitl( 0x0E );
    send16z8( 0x6EA9 );
    send16z8( 0x80A6 );
    send16z8( 0x50A8 );
    send16z8( 0x6EF5 );
    send4bit( 0x02 );
    send8bitl( 0x00 );
    if( memsiz & 0x01 ) wordtosave.n8[0] = read8bitl();
    else {
      wordtosave.n8[1] = read8bitl();
      rssend16univ( wordtosave.n16 );
    }
    ++adr;
  }
}

/* ------------------------------------ */

uint16_t sygn18f1220( void ){
  union union_16_8 sygn;
  loadadr( ADR_REVID );
  sygn.n8[0] = readflash18f1220one();
  sygn.n8[1] = readflash18f1220one();
  return sygn.n16;
}

/* ------------------------------------ */

static void idl18f1220( void ){
  prdat.progstadr16 = 0x0000;
  sendhighadr( 0x0020 );
  loadadr( 0x200000 );
  readflash18f1220( 8 );
}

/* ------------------------------------ */

static void conf18f1220( void ){
  sendhighadr( 0x0030 );
  loadadr( 0x300000 );
  readflash18f1220( 14 );
}

/* ------------------------------------ */

void read18f1220fun( void ){
  prdat.progstadr16 = 0;
  read18f1220(0x1000);
  idl18f1220();
  conf18f1220();
  readdata18f1220( 256 );
  hexend();
}

/* ------------------------------------ */
/* ERASE
 * 0x80 CHIP
 * 0x81 EEPROM
 * 0x83 BOOT
 * 0x88 BLOCK 0
 * 0x89 BLOCK 1
 * 0x8A BLOCK 2
 * 0x8B BLOCK 3
 * */

void erase18f1220what( uint8_t what ){
  
  loadadr( 0x3C0004 );
  send4bit( 0x0C );
  send8bitl( what );
  send8bitl( 0 );
  
  /* NOP */
  send4bit( 0x00 );
  sendzer();
  
  send4bit( 0x00 );
  /* wait erase 10.005 ms */
  delay_x1ms(13);
  sendzer();
}

/* ------------------------------------ */

void erase18f1220( void ){
  erase18f1220what( 0x80 );
}

/* ------------------------------------ */

void erase18f1220noconf( void ){
  erase18f1220what( 0x88 );
  erase18f1220what( 0x89 );
  erase18f1220what( 0x8A );
  erase18f1220what( 0x8B );
  erase18f1220what( 0x81 );
}

/* ------------------------------------ */

void ck( void ){
  _delay_us(0.3);  
  SETCLK();
  _delay_us(0.3);
  CLRCLK();
}

/* ------------------------------------ */

void sequencewriteclk( void ){
  CLRDAT();
  ck();
  ck();
  ck();
  _delay_us(0.3);
  SETCLK();
  waitwithfun200us( 8 ); /* min 1ms 7*185us */
  CLRCLK();
  _delay_us(7);
  sendzer();
}

/* ------------------------------------ */

static void writecomm18f1220conf( void ){
  
  send16z8( 0x8EA6 );
  send16z8( 0x8CA6 );
  
  send16z8( 0xEF00 );
  send16z8( 0xF800 );
  
  loadadr( prdat.progstadr32 );
  
  send4bit( 0x0F );
  send8bitl( bufdata[0] );
  send8bitl( bufdata[1] );
  sequencewriteclk();
  
  send16z8( 0x2AF6 );
  
  send4bit( 0x0F );
  send8bitl( bufdata[0] );
  send8bitl( bufdata[1] );
  sequencewriteclk();
}

/* ------------------------------------ */
/*  six byte in config write last */

static void writecomm18f1220conftst( void ){
  if( prdat.progstadr32 == 0x30000A ){
    wordconf.n8[0] = bufdata[0];
    wordconf.n8[1] = bufdata[1];
  } else{
    writecomm18f1220conf();
  }
}

/* ------------------------------------ */

void writecomm18f1220( void ){
  
  send16z8( 0x8EA6 );
  send16z8( 0x9CA6 );
  
  loadadr( prdat.progstadr32 );
  
  send4bit( 0x0D );
  send8bitl( bufdata[0] );
  send8bitl( bufdata[1] );
  send4bit( 0x0D );
  send8bitl( bufdata[2] );
  send8bitl( bufdata[3] );
  send4bit( 0x0D );
  send8bitl( bufdata[4] );
  send8bitl( bufdata[5] );
  send4bit( 0x0F );
  send8bitl( bufdata[6] );
  send8bitl( bufdata[7] );
  
  sequencewriteclk();
}

/* ------------------------------------ */

void writecomm18f1220byte( void ){
  
  send16z8( 0x9EA6 );
  send16z8( 0x9CA6 );

  send4bit( 0x00 );
  send8bitl( prdat.progstadr32 );
  send8bitl( 0x0E );
  send16z8( 0x6EA9 );
  
  send4bit( 0x00 );
  send8bitl( bufdata[0] );
  send8bitl( 0x0E );
  send16z8( 0x6EA8 );
  
  send16z8( 0x84A6 );
  
  send16z8( 0x0E55 );
  send16z8( 0x6EA7 );
  send16z8( 0x0EAA );
  send16z8( 0x6EA7 );
  
  send16z8( 0x82A6 );
  send16z8( 0x0000 );
   
  send4bit( 0x00 );
  waitwithfun200us( 70 ); /* min 10ms 70*185us */
  sendzer();
  send16z8( 0x94A6 );
}

/* ------------------------------------ */

static void writbf(void ){
  uint8_t metod = prdat.metod;
  if( metod == 1 ) writecomm18f1220();
  else if( metod == 2 ) writecomm18f1220conftst();
  else if( metod == 3 ) writecomm18f1220byte();  
}

/* ------------------------------------ */

void finalize18f1220(void){

  sendadrinfo();
  writbf();
   
  if( wordconf.n16 != 0xFFFF ){
    bufdata[0] = wordconf.n8[0];
    bufdata[1] = wordconf.n8[1];
    prdat.progstadr32 = 0x30000A;
    writecomm18f1220conf();
  }
}

/* ------------------------------------ */

static uint8_t setadres18f1220( void ){
  if( veryfiadres( ADR_FLASH, chipinf.sizfbyte, 8 ) ) return 1;
  if( veryfiadres( ADR_USERID, 16, 8 ) ) return 1;
  if( veryfiadres( ADR_CONFIG, 14, 2 ) ) return 2;
  if( veryfiadres( ADR_EEPROM, 256, 1 ) ) return 3;
  return 0;
}

/* ------------------------------------ */

void checkstate18f1220( void ){
  
  if( prdat.boundwr == 0 ){
    prdat.metod = setadres18f1220();
    return;
  }
  if( prdat.progadr32 >= prdat.boundwr ){
    sendadrinfo();
    writbf();
    setstrff();
  }
  prdat.metod = setadres18f1220();
}

/* ------------------------------------ */

#endif /* __GR_18f4220==1 */

