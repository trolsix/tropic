
#ifndef __FILE_COMMANDS_H__
#define __FILE_COMMANDS_H__

enum COMMAND {
	COM_NULL=0,
	COM_CHCLR,
	COM_ERASE,
	COM_READ,
	COM_SYGN,
	COM_WRITE,
	COM_VERYFI,
	COM_OK
};

uint8_t checkcommand( void );

void freestate( void );
void finalize( void );
void sendok( void );
void comminit(void);
void makecommand(void);
uint8_t readcommand( char *buf );

void writeunivfun( void );

#endif /* __FILE_COMMANDS_H__ */

