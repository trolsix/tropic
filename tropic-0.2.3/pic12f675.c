/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>

#include "bhead.h"
#include "globaltr.h"
#include "pinout.h"
#include "util.h"
#include "uarttx.h"
#include "timecount.h"
#include "readincoming.h"
#include "hardtrans.h"
#include "pic12f675.h"

/* ------------------------------------ */

#if __GR_12f675==1

/* ------------------------------------ */

void reentry( void );

/* ------------------------------------ */

static void incadr( uint16_t adrinc ){
  while(adrinc--) send6bit( 0x06 ); /* inc adr */
}

/* ------------------------------------ */

static uint16_t read12f675one( void ){
  send6bit( 0x04 ); /* read flash */
  return send16bitl( 0x0000, 1 );
}

/* ------------------------------------ */

static void read12f675( uint16_t memsiz ){
  numdat = 0;
  prdat.progsiz = memsiz*2;
  while( memsiz-- ) {
    rssend16univ( read12f675one() );
    send6bit( 0x06 ); /* inc adr */
  }
}

/* ------------------------------------ */

static void readdata12f675( uint16_t memsiz ){
  numdat = 0;
  prdat.progsiz = memsiz*2;
  while( memsiz-- ) {
    send6bit( 0x05 ); /* read data */
    rssend16univ( 0xFF & send16bitl( 0x0000, 1 ) );
    send6bit( 0x06 ); /* inc adr */
  }
}

/* ------------------------------------ */

static void loadconf(void){
  send6bit( 0x00 ); /* load conf 0x2000 */
  send16bitl( 0x7FFE, 0 );
}

/* ------------------------------------ */

uint16_t sygn12f675( void ){
  reentry();
  loadconf();
  incadr(6);        /* 0x2006 */
  return read12f675one();
}

/* ------------------------------------ */

STR_FLASH(badosccal,      "set osc 0x77\n");

static uint16_t osccal, cfgb;

void read12f675oscallbg( void ){
  reentry();
  incadr(0x3FF);
  osccal = read12f675one();
  if( 0x3400 != ( osccal & 0x3C00 )){
    /* raport bad osccal set default */
    sendstr_P(badosccal);
    osccal = 0x3477;
  }
  loadconf();
  incadr(7);
  cfgb = read12f675one() & 0x3000;
}

/* ------------------------------------ */

static void write12f675flashword( uint16_t dat ){
  dat &= 0x7FFF;
  send6bit( 0x02 );        /* load data flash */
  send16bitl( dat<<1, 0 ); 
  send6bit( 0x08 );        /* write */
  waitwithfun200us( 40 );  /* min 6ms 50*185us */
}

/* ------------------------------------ */

static void goadres( void ){
  uint16_t stadr, adr;
  stadr = prdat.progstadr16 >> 1;
  adr = prdat.progstadr32;
  prdat.progstadr16 = adr;
  adr >>= 1;
  incadr( adr - stadr );
}

/* ------------------------------------ */

#define ADR_FLASH   0x0000UL
#define ADR_USERID  0x4000UL
#define ADR_CONFIG  0x400EUL
#define ADR_EEPROM  0x4200UL

/* ------------------------------------ */

static void adrchipset( void ){
  uint8_t fl, ft;
  
  fl = prdat.progstadr16 >> 8;
  ft = prdat.progstadr32 >> 8;
  
  if( fl != ft ) {
    if( ft == (ADR_EEPROM>>8) ){
      reentry();
      prdat.progstadr16 = ADR_EEPROM;
    } else if( ft == (ADR_USERID>>8) ){
      loadconf();
      prdat.progstadr16 = ADR_USERID;
    }
  }
  goadres();
}

/* ------------------------------------ */

static void write12f675eeprom( void ){

  wordtosave.n8[0] = bufdata[0];
  wordtosave.n8[1] = 0x00;
  
  send6bit( 0x03 ); /* load data eeprom */
  send16bitl( wordtosave.n16<<1, 0 ); 
  send6bit( 0x08 ); /* write */
  delay_x1ms(8);    /* min 6 ms */
}

/* ------------------------------------ */

static void write12f675conf( void ){
  wordconf.n8[0] = bufdata[0];
  wordconf.n8[1] = bufdata[1];
}

/* ------------------------------------ */

void read12f675fun( void ){
  
  reentry();
  prdat.progstadr16 = 0;
  read12f675( 1023 );
  
  loadconf();
  prdat.progstadr16 = 0x4000;
  read12f675(4);    //id

  loadconf();
  prdat.progstadr16 = 0x400E;
  incadr(7);
  read12f675(1);    //conf
  
  prdat.progstadr16 = 0x4200;  
  reentry();
  readdata12f675( 128 );

  hexend();
}
  
/* ------------------------------------ */

static void write12f675flashb( void ){
  union union_16_8 dat;
  dat.n8[0] = bufdata[0];
  dat.n8[1] = bufdata[1];
  write12f675flashword(dat.n16);
}

/* ------------------------------------ */

static void wrwri( void ){
  uint8_t metod = prdat.metod;
  adrchipset();
  if( metod == 1 ) write12f675flashb();
  else if( metod == 2 ) write12f675conf();
  else if( metod == 3 ) write12f675eeprom();
}

/* ------------------------------------ */

void finalize12f675fun(void){
  
  wrwri();
  
  if( wordconf.n16 != 0xFFFF ){
    reentry();
    loadconf();
    incadr(7);
    write12f675flashword(wordconf.n16);
  }
}

/* ------------------------------------ */

static uint8_t setadres12f675( void ){
  if( veryfiadres( ADR_FLASH, 0x400 * 2, 2 ) ) return 1;
  if( veryfiadres( ADR_USERID, 8, 2 ) ) return 1;
  if( veryfiadres( ADR_CONFIG, 2, 2 ) ) return 2;
  if( veryfiadres( ADR_EEPROM, 256, 2 ) ) return 3;
  return 0;
}

/* ------------------------------------ */

void checkstate12f675( void ){
  
  uint16_t prnzop, prbop;
  
  prbop = prdat.boundwr;
  
  if( prbop == 0 ){
    reentry();
    prdat.metod = setadres12f675();
    return; /* must be for optymalized 16 bit */
  }
  prnzop = prdat.progadr32;

  if( prnzop >= prbop ){
    sendadrinfo();
    wrwri();
    setstrff();
  }
  prdat.metod = setadres12f675();
}

/* ------------------------------------ */

void erase12f675( void ){
  read12f675oscallbg();
  reentry();
  loadconf();       /* load conf 0x2000 */
  send6bit( 0x09 ); /* bulk erase program */
  delay_x1ms(12);   /* min 8 ms */
  send6bit( 0x0B ); /* bulk erase data */
  delay_x1ms(12);
  /* restore oscall and BG */
  
  reentry();
  incadr(0x3FF);
  write12f675flashword(osccal);
  loadconf();
  incadr(7);
  write12f675flashword(cfgb | 0x0FFF);
}

/* ------------------------------------ */

#endif /* __GR_12f675 */

