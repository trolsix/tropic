#<meta http-equiv="content-type" content="text/html; charset=utf-8">
#<?xml version='1.0' encoding='utf-8'?>

Po włączeniu układu napięcie na kondensatorze C3 winno wynosić 9V,
kalibracji dokonujemy rezystorami R7 i R11

R9 dobieramy na prąd w kolektorze o wartości 100mA

R3 dobieramy aby przy zwartym R2 prąd kolektora Q1 wynosił max 500mA
R2 dobieramy aby prąd kolektora Q1 wynosił ok 20mA

R28 dobieramy aby prąd kolektora Q7 wynosił ok 15mA
R14 dobieramy aby prąd kolektora Q5 wynosił ok 20mA

