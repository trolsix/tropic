#<meta http-equiv="content-type" content="text/html; charset=utf-8">
#<?xml version='1.0' encoding='utf-8'?>

------------------------------

Tropic wersja 0.2.3:

dodano obsługe standardowego sprzętowego przepływu CTS RTS
ponieważ jak się okazuje standardowy terminal w linuxie nie obsługuje
programowego protokołu xonxoff dla niektórych przejściówek USB-RS232

Linie RTS jednego urządzenia łączymy z CTS i CTS jednego urządzenia łączymy z RTS.

------------------------------

Linux

W systemie linux standardowo RS232 jest obsługiwany jako terminal.
Ma on jednak całkiem sporo ustawień. Ustawienia można odczytać poleceniem stty.
Przykładowo dla COM1:

stty -F /dev/ttyS0 -a

Należy powyłaczać echa które z jakiegoś powodu są:

stty -F /dev/ttyS0 -echo -echoe -echok -echonl -echoprt -echoctl -echoke

Włączenie protokołu xon-xoff:

stty -F /dev/ttyS0 ixon

Włączenie sprzętowego sterowania przepływem:

stty -F /dev/ttyS0 crtscts

Ustawienie szybkości:

stty -F /dev/ttyS0 speed 57600

------------------------------

https://www.eevblog.com/forum/microcontrollers/hardware-flow-control-in-usb-cdc/

RS232 has always been a can of worms...

No hardware flow control:
1) Most (decent) USB-Serial dongles have RTS/CTS. If you're making one, be sure to provide RTS/CTS.
2) Most USB-Serial dongles have hardware flow control AND IT'S ALWAYS ENABLED AND CAN'T BE DISABLED.
3) Most serial devices *used* to have RTS/CTS signal pins, but nowadays not so often.
4) /CTS is the (important!) input that when LOW means "the other end is ready for more".
5) /CTS is driven by the other end RTS.
6) To disable hardware flow control just leave /CTS disconnected and make sure that it always stays LOW, at both ends, no matter what.
7) Some devices read LOW at /CTS when it's left unconnected, some don't. Watch out for that and act accordingly.
8) RTS doesn't matter at all for this to work and can be ignored. NC.

Hardware flow control:
1) Just connect the DTR of one end to the /CTS of the other end, and viceversa, and it will work.
2) The hardware flow control of most (decent) USB-Serial dongles is ALWAYS ENABLED because it has a buffer that can be overflowed w/o it. If you want to take that risk, up to you, see above and leave /CTS disconnected (and make sure it's LOW).

The thing is, many, many devices have shallow RX buffers (e.g. ATMEGAs have two bytes)
and need a way to cut the traffic when/if they can't keep up for any reason
(e.g. 250000 baud and noInterrupts() ?). It's better to connect and use DTR/CTS flow control,
unless the upper layer protocol can detect overruns, which isn't always the case.

------

115200 xon-xoff protocole for send

https://andym3.wordpress.com/mini-howto-linux-windows-serial-file-transfer/
https://stackoverflow.com/questions/36443169/how-to-send-file-over-serial-port-in-windows-command-prompt
https://kermitproject.org/k95.html
https://en.wikipedia.org/wiki/RS-232#Sp%C3%A9cification

------
