/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 /*-----------------------------------------------*/
/*
 * stack min 1068 (dec) max 1024+(32+64)=1120
 * 0x021e data 512+16+15+96(start)=639
 */
/*-----------------------------------------------*/

#include <stdint.h>
#include <string.h>

/*-----------------------------------------------*/

#if (__AVR_ARCH__ > 1)
#include <compat/ina90.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
/*
#include <avr/sleep.h> 
*/
#include <avr/wdt.h>
#endif

/*-----------------------------------------------*/
/*   configuracje fuse i configi hardware uC     */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/
/*               software h                      */
/*-----------------------------------------------*/

#include "globaltr.h"
#include "util.h"
#include "uarttx.h"
#include "commandrs.h"
#include "timecount.h"
#include "readincoming.h"

/*-----------------------------------------------*/

//STR_FLASH(version,"tropic: auth:trol.six , GPL v2 license, 0.2.3\n");
STR_FLASH(version,"tropic auth: trol.six GPL v2 v.0.2.3\n");

/*-----------------------------------------------*/

void initport(void);
void addfun(void(*fun)(void));

/*-----------------------------------------------*/

void (*wskfunglob)(void);
void (* tblfunglob[8])(void);
uint8_t wskfunnmbw;
//uint8_t wskfunnmbr;

/*-----------------------------------------------*/

char * wsp;
char tbl[11];
uint32_t adrout32;

/*-----------------------------------------------*/

//volatile uint8_t delaytmp;

/*-----------------------------------------------*/

void showver( void ) {
	sendstr_P(version);
}

/*-----------------------------------------------*/
/*
void getbinstr( uint32_t data, uint8_t numb ){
	binbcd1( tbl, data );
  RSouttbl( numb, tbl );
  RSout(' ');
}
*/
/*-----------------------------------------------*/

#if __SHOW_ADC==1

void sendvoltage( void ) {
	getbinstr( adcres[0], 5 );
	getbinstr( adcres[1], 5 );
  RSout('\n');
}

#endif

/*-----------------------------------------------*/

void funtimesend( void ){
	showalltime();
}

/*-----------------------------------------------*/

uint32_t tmpmssec;

int main( void ){

  uint8_t wskfunnmbr;

  wdt_reset();
  
#if __TIME_GLOB__==1
  mssec = 0;
#endif

#if __TIME_ADCCOOUNT__==1
  tirqadc = 0;
#endif

  initport();
  //flag = 0; /* slould be */
  flag |= XOFFSEND;

  memset( (void*) &tblfunglob, 0x00, sizeof(tblfunglob) );
  wskfunnmbw = 0;
  wskfunnmbr = 0;
	tmpmssec = 0;
  voltageznorm = 1144; /* 9V, 1560  12V */
	
  delay_x1ms(10);
  wdt_reset();
  delay_x1ms(10);
  wdt_reset();
  
  comminit();
	uart_init();
  initincom(); /* enable sei */
  
	showver();
  
	while(1){
		
		//wdt_reset();

#if __TEST_INCOMING==1
    
		if( tmpmssec != mssec ){
			++tmpmssec;

			adcres[0] = TCNT1;
			adcres[1] = TCNT1;

      //if( ( 0xFFF & tmpmssec ) == 0x10 )
        //testsendcom("CHIP 18f25q10\nSYGN_OFF\nWRITE\n");
        
      //if( ( 0xFFF & tmpmssec ) == 0x10 )
        //testsendcom("CHIP 18f1220\nSYGN_OFF\nREAD\n");
          
      //if( ( 0xFFF & tmpmssec ) == 0x10 )
        //testsendcom("CHIP 12f675\nSYGN_OFF\nREAD\n");
        
      //if( ( 0xFFF & tmpmssec ) == 0x10 )
        //testsendcom("CHIP_LIST\naEND\nHELP\n");
        /*
      if( ( 0xFF & tmpmssec ) == 0x10 )
        testsendcom("asadfasdfasdf1\n");
      if( ( 0xFF & tmpmssec ) == 0x20 )
        testsendcom("asadfasdfasdf2\n");
      if( ( 0xFF & tmpmssec ) == 0x30 )
        testsendcom("asadfasdfasdf3\n");
      if( ( 0xFF & tmpmssec ) == 0x40 )
        testsendcom("asadfasdfasdf4\n");
      if( ( 0xFF & tmpmssec ) == 0x50 )
        testsendcom("asadfasdfasdf5\n");*/
        
        /* conf 12f675 */
      //if( ( 0xFFF & tmpmssec ) == 0x30 )
        //testsendcom(":02400E00B431CB\n");
        
      //if( ( 0xFFF & tmpmssec ) == 0x10 )
        //testsendcom("CHIP 18f1220\nSYGN_OFF\nREAD\n");
        
      if( ( 0xFFF & tmpmssec ) == 0x10 )
        testsendcom("CHIP 18f27q10\nSYGN_OFF\nREAD\nHELP\n");
      
      //if( ( 0xFFF & tmpmssec ) == 0x20 )
        //testsendcom(":100018000A80E0CF08F008C0E0FF0A90100015EF52\n");
        
      //if( ( 0xFFF & tmpmssec ) == 0x20 )
        //testsendcom(":0200000400F00A\n");
      //if( ( 0xFFF & tmpmssec ) == 0x30 )
        //testsendcom(":10000000000102030405060708090A0B0C0D0E0F78\n");

        /* eeprom 12f675 */
      //if( ( 0xFFF & tmpmssec ) == 0x40 )
        //testsendcom(":10420000FF00010002000300040005000600070093\n");
        
        
      if( ( 0xFFF & tmpmssec ) == 0x40 ) testsendcom("END\n");
        
        /* 25q10 copnfig  */
      //if( ( 0xFFF & tmpmssec ) == 0x040 )
        //testsendcom(":020000040030CA\n:02000800FFFFF8\n");
        
        /* 25q10 eeprom  */
      //if( ( 0xFFF & tmpmssec ) == 0x040 )
        //testsendcom(":020000040031C9\n:08000000FF01020304050607DD\n");

      //if( ( 0xFFF & tmpmssec ) == 0x050 )
        //testsendcom(":10007800742024302A05A70020302706031D4628AF\n");
        
      /*if( ( 0xFFF & tmpmssec ) == 0x070 ) {
        testsendcom(":020000040030CA\n");
        testsendcom(":0E000000FF080C1AFF8081FF03C003E00340DD\n");
      }*/
        
        /* 1220 eeprom  */
      /*if( ( 0xFFF & tmpmssec ) == 0x090 ) {
        testsendcom(":0200000400F00A\n");
        testsendcom(":08000000FF01020304050607DD\n");
      }*/

      /*if( ( 0xFFF & tmpmssec ) == 0x090 )
        testsendcom(":02400E00B431CB\n");*/
      /*if( ( 0xFFF & tmpmssec ) == 0x0A0 )
        testsendcom(":10420000FF00010002000300040005000600070093\n");*/

      if( ( 0xFFF & tmpmssec ) == 0x100 )
        testsendcom(":00000001FF\n");
      /*          
      if( ( 0xFFF & tmpmssec ) == 0x200 ) {
        testsendcom("CHIP 12f675\n");
        testsendcom("SYGN_OFF\n");
        testsendcom("READ\n");
        
      }*/
		}
    
#endif
    
    funincombuf();
    
    if( tblfunglob[wskfunnmbr] ){
      tblfunglob[wskfunnmbr]();
      tblfunglob[wskfunnmbr] = NULL;
      ++wskfunnmbr;
      wskfunnmbr &= 0x07;
    }

	}

	return 0;
}

/*-----------------------------------------------*/

void addfun( void(*fun)(void) ){
  tblfunglob[wskfunnmbw] = fun;
  ++wskfunnmbw;
  wskfunnmbw &= 0x07;
}

/*-----------------------------------------------*/
/* irq avra */
/*-----------------------------------------------*/

ISR(TIMER0_OVF_vect){
	//if(delaytmp)--delaytmp;
	TCNT0 = TCNT0-108;
#if __TIME_GLOB__==1
	++mssec;
#endif
}

/*-----------------------------------------------*/
/*
ISR(TIMER1_OVF_vect) {

}
*/
/*-----------------------------------------------*/
/*     PWM 255 MAX 200 IN 0                      */
/*     ADC 0-16V                                 */
/*-----------------------------------------------*/

static int16_t adcbefore;

const uint8_t dmod[16] PROGMEM = {
   0,  36,  67,  92, 114, 132, 147, 160,
 171, 180, 188, 194, 199, 204, 208, 211 };

ISR(ADC_vect) {

#if __TIME_ADCCOOUNT__==1
  uint8_t tmp, tmp2;
  tmp = TCNT1;
#endif

  wdt_reset();
  
  if( 0x06 == ( ADMUX & 0x07 ) ){
    adcres[adcvcc] = ADC;
    ADMUX = (3<<REFS0)|(0x07);
  } else {
    int16_t adctmp;
    uint8_t tmpcal;
    
    adctmp = ADC;
    ADMUX = (3<<REFS0)|(0x06);
    
    adctmp = adcbefore + adctmp;
    adcres[adcvhh] = adctmp;
    adcbefore = ADC;
    adctmp = voltageznorm - adctmp;
    if( adctmp < 0 ) adctmp = 0;
    else if( adctmp > 15 ) adctmp = 15;
    
    tmpcal = adctmp;
    adctmp = pgm_read_byte( &dmod[tmpcal] );
    OCR1A = adctmp;
  }
  
  ADCSRA |= (1<<ADSC);

#if __TIME_ADCCOOUNT__==1
  tmp2 = TCNT1;
  tmp2 -= tmp;
  if( tmp2 > tirqadc) tirqadc = tmp2;
#endif
}

/*-----------------------------------------------*/

#if __TIME_COOUNT__==1
extern uint32_t tick2;
#endif

/*-----------------------------------------------*/
/* PC2 input CTS PC3 RTS */

#define PRESCADC64   (6<<ADPS0)
#define PRESCADC32   (5<<ADPS0)
#define PRESCADC16   (4<<ADPS0)
#define PRESCADC     PRESCADC32

#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega168__)

void initport (void){

	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x03 | 0x40; /* txd rxd vccen */
  //DDRC  = 0x3F;
  DDRC  = (1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5);
	DDRD  = (1<<1)|(1<<2)|(1<<4)|(1<<5)|(1<<6);
  //DDRD  = (1<<1)|(1<<4)|(1<<5)|(1<<6);
	DDRB  = (1<<1)|(1<<2); /* PWM1 A B */
	
/* watchdog */
	wdt_enable(WDTO_15MS);

/* ADC ref 1.11 adc ch 6*/
  ADMUX = (3<<REFS0)|(0x06);
  
/* IRQ en 32 presc 11059200 / 64 /14 = 24685/2 */
  ADCSRA = (1<<ADEN)|(1<<ADIE)|PRESCADC;
  ADCSRA |= (1<<ADSC);
  
/* fast PWM ICR1 43,2kHz 0x00FF - WGM 1110 */
	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
	TCCR1B = (1<<WGM13)|(1<<WGM12)|(1<<CS10);
	ICR1 = 0x00FF;
	OCR1A = 0x000F;
	OCR1B = 0x000F;

  TCCR2B = (1<<CS21); //8 presc

/* T0 1024 perscaler irq 255-108 100hz */
	TCCR0B = (5<<CS00);
  
	TIMSK0 = (1<<TOIE0); /* t0 t2 irq */
	TIMSK2 = (1<<TOIE2); /* t0 t2 irq */

#if __TIME_COOUNT__==1
  tick2 = 0;
  savesp.n16 = 0xFFFF;
#endif

	return;
}

#endif

/*-----------------------------------------------*/

#if defined(__AVR_ATmega8__)

void initport (void){

	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x03 | 0x40; /* txd rxd vccen */
  //DDRC  = 0x3F;
  DDRC  = (1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5);
	DDRD  = (1<<1)|(1<<2)|(1<<4)|(1<<5)|(1<<6);
	//DDRD  = (1<<1)|(1<<4)|(1<<5)|(1<<6);
	DDRB  = (1<<1)|(1<<2); /* PWM1 A B */
	
/* watchdog */
	wdt_enable(WDTO_15MS);

/* ADC ref 2.56 adc ch 6*/
  ADMUX = (3<<REFS0)|(0x06);
  
/* IRQ en 32 presc 11059200 / 32 /14 = 24685 */
//  ADCSRA = (1<<ADEN)|(1<<ADIE)|(5<<ADPS0);
/* IRQ en 32 presc 11059200 / 64 /14 = 24685/2 */
  ADCSRA = (1<<ADEN)|(1<<ADIE)|PRESCADC;
  ADCSRA |= (1<<ADSC);
  
/*
fast PWM ICR1 21,6kHz 0x01FF
fast PWM ICR1 43,2kHz 0x00FF
WGM 1110
*/
	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
	TCCR1B = (1<<WGM13)|(1<<WGM12)|(1<<CS10);
	ICR1 = 0x00FF;
	OCR1A = 0x000F;
	OCR1B = 0x000F;

  /* TCCR2 = (1<<CS21)|(1<<CS20); */ /*32 presc*/
  TCCR2 = (1<<CS21); //8 presc

/* fast PWM 256 43,2kHz */
/*	TCCR2 = (1<<WGM20)|(1<<WGM21)|(1<<COM21)|(1<<CS20); */

/* T0 1024 perscaler irq 255-108 100hz */
	TCCR0 = (5<<CS00);
  
	//TIMSK = (1<<TOIE0)|(1<<TOIE1);
	TIMSK = (1<<TOIE0)|(1<<TOIE2); /* t0 t2 irq */

#if __TIME_COOUNT__==1
  tick2 = 0;
  savesp.n16 = 0xFFFF;
#endif

	return;
}

#endif

/*-----------------------------------------------*/

