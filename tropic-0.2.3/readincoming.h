
#ifndef __FILE_READINCOMING_H__
#define __FILE_READINCOMING_H__

#include "globaltr.h"

void comrst( void );
void funincombuf( void );
void initincom( void );
void waitwithfun200us( uint8_t timew );

void setflagatomic( uint8_t set );
void clrflagatomic( uint8_t set );

#if __TEST_COMMAND==1
void testcommand( char * str );
#endif
	
#if __TEST_INCOMING==1
void testsendcom( const char * );
#else
#define testsendcom()
#endif


#endif /* __FILE_READINCOMING_H__ */
