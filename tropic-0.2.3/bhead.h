
#ifndef __FILE_BHEAD_H__
#define __FILE_BHEAD_H__

#define __USED__EEPROM    0
#define __RTSCTS          1

/* 1.3 1.6 1.1 */
#define __GR_18f47q10     1
#define __GR_18f4220      1
#define __GR_12f675       1
#define __GR_tiny25       0

#define __SHOW_TIME       1
#define __SHOW_ADC        0

#define __TEST_INCOMING   0
#define __TEST_COMMAND    0

#define __XONXOFF_DIS     0

#if (__AVR_ARCH__ > 1)
#include <avr/pgmspace.h>
#else
#define PROGMEM
#endif

#if defined(__AVR_ATmega8__)
#define _REG_TCCR2         TCCR2
#define _REG_UART0         UDR
#define _UART0_ISEMPTY     ( UCSRA & (1<<UDRE) )
#define _ISR_USART_VECT()  ISR(USART_RXC_vect)
#define _ISR_USART_FUN()   USART_RXC_vect()
#define _EN_UART_EMPTY()   UCSRB |= 1<<UDRIE
#define _DIS_UART_EMPTY()  UCSRB &= ~(1<<UDRIE)
#endif

#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega168__)
#define _REG_TCCR2         TCCR2B
#define _REG_UART0         UDR0
#define _UART0_ISEMPTY     ( UCSR0A & (1<<UDRE0) )
#define _ISR_USART_VECT()  ISR(USART_RX_vect)
#define _ISR_USART_FUN()   USART_RX_vect()
#define _EN_UART_EMPTY()   UCSR0B |= 1<<UDRIE0
#define _DIS_UART_EMPTY()  UCSR0B &= ~(1<<UDRIE0)
#endif

#endif /* __FILE_BHEAD_H__ */

