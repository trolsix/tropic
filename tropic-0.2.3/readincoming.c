/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------*/

#include <stdint.h>
#include <string.h>

/*-----------------------------------------------*/

#if (__AVR_ARCH__ > 1)
#include <avr/io.h>
#include <compat/ina90.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/wdt.h>
#endif

#include "globaltr.h"
#include "bhead.h"
#include "pinout.h"
#include "commandrs.h"
#include "util.h"
#include "uarttx.h"
#include "timecount.h"
#include "readincoming.h"

/*-----------------------------------------------*/

static void funincom( void );

/*-----------------------------------------------*/

void addfun( void(*fun)(void));
void funtimesend( void );

/*-----------------------------------------------*/
/*    bufor for incoming data    */

#if defined(__AVR_ATmega328__)
#define SIZBUFUARTIN   1024
#else
#define SIZBUFUARTIN   (15*16)
#endif

char bufincomin[SIZBUFUARTIN];

#if defined(__AVR_ATmega328__)
typedef uint16_t tincoming;
#else
typedef uint8_t tincoming;

#endif

tincoming binin, binou;
volatile tincoming fillbuf;

#define FILL_XON_XOFF  ( SIZBUFUARTIN - 32 )

/*-----------------------------------------------*/

static char rsbuf0[16];

/*-----------------------------------------------*/

STR_FLASH(strbadcrc,  " bad crc\n");
STR_FLASH(strbadtype, " bad typ\n");
STR_FLASH(strbadform, " bad hex\n");
STR_FLASH(strbadcomm, " bad command\n");
//STR_FLASH(strbufful,  " buf full\n");
STR_FLASH(strtofast,  " too fast!\n");

/*-----------------------------------------------*/

static uint8_t  iledat;
static uint8_t iledatincom;
static uint8_t  type;
static uint8_t  crc; /* maybe gobal ? */
static uint16_t adrin16;
static uint32_t adrin32;
static uint8_t  strhex;

/*-----------------------------------------------*/
/* test funkcion */

#if __TEST_COMMAND==1
static char rsbuf1[32];

void testcommand( char * str ){
	uint8_t co;
  
	strcpy( rsbuf1, str );
	co = readcommand( rsbuf1 );
	RSout( '#' );
	RSoutstr( (const char*)rsbuf1 );
	RSout( ' ' );
	if(co == COM_NULL) sendstr_P(strbadcomm);
	return;	
}
#endif

/*-----------------------------------------------*/

enum STATE_RS {
	STRS_NULL,
	STRS_STR,
	STRS_NMBB,
	STRS_ADR,
	STRS_TYPE,
	STRS_DATA,
	STRS_CRC,
	STRS_WAITEOL,
	STRS_END
};

/*-----------------------------------------------*/

static uint8_t prs;
static uint8_t strs;
static char datars;

void setflagatomic( uint8_t set ){
  cli();
  flag |= set;
  sei();
}

void clrflagatomic( uint8_t set ){
  cli();
  flag &= ~set;
  sei();
}

/*-----------------------------------------------*/

static void fun_STRS_NULL( char dat ){
    
  prs = 1;
	if( dat <= ' ' ){
		strs = STRS_NULL;
	} else	if( dat == ':' ) {
		strs = STRS_NMBB;
    setflagatomic( HEXACT );
    crc = 0;
    iledat = 0;
    adrin16 = 0;
    iledatincom = 0;
    type = 0;
    return; 
	} else {
		strs = STRS_STR;
    rsbuf0[0] = dat;
    return;
	}
  prs = 0;
}

/*-----------------------------------------------*/

static void fun_STRS_NMBB( uint8_t dat ){

  iledat = dat;
  crc += dat;
  strs = STRS_ADR;
  prs = 1;
  adrin16 = 0;
}

/*-----------------------------------------------*/

static void fun_STRS_ADR( uint8_t dat ){
  
	if( prs == 5 ){
		crc += dat;
		adrin16 = (adrin16<<8) + dat;
		strs = STRS_TYPE;
		prs = 1;
	} else if ( prs == 3 ) {
		crc += dat;
		adrin16 = dat;
	}
}

/*-----------------------------------------------*/

static void fun_STRS_TYPE( uint8_t dat ){

  crc += dat;
  type = dat;
  strs = STRS_DATA;
  prs = 1;
  if( type == 4 ) adrin32 = 0;
  if( type == 2 ) adrin32 = 0;
  if( type == 1 ) strs = STRS_CRC; /* maybe how much data */
}

/*-----------------------------------------------*/
/* 9778 ---- 8172 8158 8058 */

static void fun_STRS_DATA( uint8_t strhex ){
	
	uint8_t sizdatinc;
	uint32_t adrin32tmp;
	
	sizdatinc = iledatincom;
	adrin32tmp = adrin32;
	
  crc += strhex;
	adrin32tmp <<= 8;
  adrin32tmp += strhex;
	
  if( type == 0 ){
    bufram[idxbufin] = strhex;
    ++idxbufin;
    idxbufin &= BUFRAMMSK;
    if( ++sizdatinc == iledat ) strs = STRS_CRC;
  } else if( type == 2 ){
    if( ++sizdatinc == iledat ){
      /* adrin32tmp *= 16; */
      adrin32tmp <<= 4;
			adrin32 = adrin32tmp;
      strs = STRS_CRC;
    }
  } else if( type == 4 ){
    if( ++sizdatinc == iledat ) {
      adrin32tmp <<= 16;
			adrin32 = adrin32tmp;
      strs = STRS_CRC;
    }
  } else {
    sendstr_P(strbadtype);
    strs = STRS_NULL;
  }
	
	iledatincom = sizdatinc;
  prs = 1;
}

/*-----------------------------------------------*/

static void fun_STRS_CRC( uint8_t strhex ){
  
  crc = 0x00 - crc;
  
  if( !( flag & ENDWAIT ) ){
    if( crc != strhex ){
      sendstr_P(strbadcrc);
      setflagatomic( ENDWAIT );
      idxbufou = idxbufin;
    } else {
      /* in finsh program rather don't send this */
      /* only comunicate witch write veryfi */
      if(( type == 0 ) && ( checkcommand() ) ){
          prdat.progadr32 = adrin32 + adrin16;
          prdat.progadr16 = adrin16;
          prdat.progsiz = iledat;
          addfun(makecommand);
      }
      if( type == 1 ){
        addfun(freestate);
        idxbufou = idxbufin;
        prdat.progsiz = 0;
      }
    }
  }
  clrflagatomic(HEXACT);
  strs = STRS_WAITEOL;
  /* strs = STRS_NULL; */
  prs = 0;
}

/*-----------------------------------------------*/

static void fun_STRS_STR( char tmp ){

  uint8_t command;
  
	if( (tmp=='\n') || (tmp=='\r') ) {
    rsbuf0[prs] = 0;
    command = readcommand( rsbuf0 );
    /* after ok 100ms ? */
		if(command == COM_NULL) sendstr_P(strbadcomm);
    else clrflagatomic( ENDWAIT );
		strs = STRS_NULL;
    prs = 0;
		return;
	}
	rsbuf0[prs] = tmp;
  
	if( ++prs >= sizeof(rsbuf0) ){
		sendstr_P(strbadcomm);
		//sendstr_P(strbufful);
		prs = 0;
		strs = STRS_WAITEOL;
	}
}

/*-----------------------------------------------*/

static void fun_STRS_WAITEOL( char tmp ){
	if( (tmp=='\n') || (tmp=='\r') ) {
		strs = STRS_NULL;
    prs = 0;
		return;
	}
}

/*-----------------------------------------------*/

void comrst( void ){
  adrin32 = 0;
}

/*-----------------------------------------------*/

static void sendxon( void ){
  
  clrflagatomic(XONXOFF|XOFFSEND); /* enable sei */
  
  //while((flag & XOFFSEND) || (0 == _UART0_ISEMPTY));
  while( 0 == _UART0_ISEMPTY );
  while( _IS_CTS_SET() );
  _REG_UART0 = 17;
#if __RTSCTS==1
  PORT_RTS &= ~(1<<PIN_RTS);
#endif
  
}

/*-----------------------------------------------*/

void initincom( void ){
  
  prs = 0;
  strs = STRS_NULL;
	
	binin = 0;
	binou = 0;
  fillbuf = 0;
  idxbufin = 0;
  idxbufou = 0;
  
  sendxon();

  _REG_UART0;
  
  //clrflagatomic(HEXACT|ISDATA|BUFINOV|XONXOFF);
  clrflagatomic(HEXACT|BUFINOV|XONXOFF);
}

/*-----------------------------------------------*/

void funincombuf( void ){

  int16_t tmpbuffill;
  tincoming tmpfb;
  
  cli();
  tmpfb = fillbuf;
  sei();
      
  if( tmpfb == 0 ) return;
  
	/* can ended */
	if( ( strs == STRS_CRC ) && ( prdat.progsiz ) ) return;
	
	if( prdat.progsiz ){
		tmpbuffill = idxbufin - idxbufou;
		tmpbuffill &= BUFRAMMSK;
		if( tmpbuffill == BUFRAMMSK ) return;
	}

	datars = bufincomin[binou];
	
	if( ++binou >= sizeof(bufincomin) ) binou = 0;
	
	funincom();
 
	cli(); /* atomic for fillbuf */
	tmpfb = fillbuf;
	--tmpfb;
	fillbuf = tmpfb;
	sei();
	
	if( ( flag & BUFINOV ) && ( tmpfb == 0 ) ){
		sendstr_P(strtofast);
		addfun(initincom);
		setflagatomic(ENDWAIT);
		return;
	}
	
	if( ( flag & XONXOFF ) && ( tmpfb < 32 ) )
		sendxon(); /* turns sei*/

}

/*-----------------------------------------------*/

static void funincom( void ){
  
  uint8_t tmpw;
  
  tmpw = datars;

  if( flag & HEXACT ){
    uint8_t tmp;
    
    tmp = hashexdigit( tmpw );
    
    if( 0x80 & tmp ){
      if( ( tmpw == '\n' ) || ( tmpw == '\r' ) ) strs = STRS_NULL;
      else strs = STRS_WAITEOL;
      sendstr_P(strbadform);
      clrflagatomic( HEXACT );
      setflagatomic( ENDWAIT );
      return;
    }
    strhex = (strhex<<4) + tmp;

    if( ++prs == 2 ) return;

    tmpw = strhex;
  }

  switch (strs) {
    case STRS_NULL:    fun_STRS_NULL( tmpw ); break;
    case STRS_STR:     fun_STRS_STR( tmpw ); break;
    case STRS_NMBB:    fun_STRS_NMBB(tmpw); break;
    case STRS_ADR:     fun_STRS_ADR(tmpw); break;
    case STRS_TYPE:    fun_STRS_TYPE(tmpw); break;
    case STRS_DATA:    fun_STRS_DATA(tmpw); break;
    case STRS_CRC:     fun_STRS_CRC(tmpw); break;
    case STRS_WAITEOL: fun_STRS_WAITEOL(tmpw); break;
    case STRS_END:     break;
    default: break;
  }

}

/*-----------------------------------------------*/

#if __TEST_INCOMING==1
char testuart;
#endif

/*-----------------------------------------------*/
/* xon xoff pause 19 start 17     */
/* hard rts pc3 ctc pc2           */

_ISR_USART_VECT(){
	
	char tch;

	tch = _REG_UART0;

  if( flag & BUFINOV ) return;

#if __TEST_INCOMING==1
	tch = testuart;
#endif

#if __XONXOFF_DIS==1
  if( !( flag & XOFF ) ) {
#endif
  
    if( fillbuf == FILL_XON_XOFF ){
      if(!(flag & XONXOFF)){
        flag |= XONXOFF | XOFFSEND;
#if __RTSCTS==1
        PORT_RTS |= 1<<PIN_RTS;
#endif
        _EN_UART_EMPTY();
      }
    }
  #if __XONXOFF_DIS==1
  }
  #endif
  
  if( fillbuf >= ( sizeof(bufincomin) -1 )){
    flag |= BUFINOV;
  } else {
    fillbuf += 1;
    bufincomin[binin++] = tch;
    if( binin >= sizeof(bufincomin) ) binin = 0;
  }

	return;
}

/*-----------------------------------------------*/

#if __TEST_INCOMING==1

void testsendcom( const char * commtest  ){
  while( *commtest ) {
    testuart = *commtest++;
    _ISR_USART_FUN();
  }
}

#endif

/*-----------------------------------------------*/

void waitwithfun200us( uint8_t timew ){
  
  cli();
  wait8 = timew; /* about 200us 2048 cycles 11059200 */
  sei();
  do {
    if( bufincomin[binou] >= ' ' ) funincombuf();
  } while( wait8 );
}

/*-----------------------------------------------*/

