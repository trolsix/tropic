
/*-----------------------------------------------*/

#if defined(__AVR_ATmega8__)
FUSES =
{
	/* crystal, bod enabled */
	.low = 0x9F,
	.high = 0xc9,
};
#endif

#if defined(__AVR_ATmega328__)
FUSES =
{
	/* crystal, bod enabled */
	.low = 0xFF,
	.high = 0xd9,
	.extended = 0xfd,
};
#endif

/*-----------------------------------------------*/
