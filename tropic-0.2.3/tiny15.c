/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>
#include <avr/pgmspace.h>

#include "globaltr.h"
#include "pinout.h"
#include "bhead.h"
#include "uarttx.h"
#include "hardtrans.h"
#include "timecount.h"
#include "tiny15.h"

/* ------------------------------------ */

#if __GR_tiny25==1

/* ------------------------------------ */

static uint8_t chip1315;

/* ------------------------------------ */

static void eight( void ){
  uint8_t i;
  if(chip1315){
    SETCLK();
    _delay_us(0.5);
    CLRCLK();
    _delay_us(0.5);
    return;
  }
  for( i=16; i; --i ){
    SETCLK();
    _delay_us(0.5);
    CLRCLK();
    _delay_us(0.5);
  }
}

/* ------------------------------------ */

void set( uint8_t d) {
  chip1315 = d;
}

/* ------------------------------------ */

void locktiny15( void ){
  numdat = 0;
  _delay_us(10);
  serialtiny15( 0x04, 0x4C );
  serialtiny15( 0x00, 0x78 );
  rssend8( serialtiny15( 0x00, 0x7C ) );
}

/* ------------------------------------ */

void fusetiny15( void ){
  numdat = 0;
  _delay_us(10);
  serialtiny15( 0x04, 0x4C );
  serialtiny15( 0x00, 0x68 );
  rssend8( serialtiny15( 0x00, 0x6C ) );
}

/* ------------------------------------ */

uint32_t sygntiny15( void ){

  union union_32_8 sygn;
  uint8_t i;
  
  numdat = 0;
  _delay_us(10);
  
  for( i=0; i<3; ++i ){
    serialtiny15( 0x08, 0x4C );
    serialtiny15( i, 0x0C );
    serialtiny15( 0x00, 0x68 );
    sygn.n8[i] = serialtiny15( 0x00, 0x6C );
    rssend8( sygn.n8[i] );
  }
  
  RSout('\n');
  return sygn.n32;
}

/* ------------------------------------ */

void entertiny15( void ){

  CLRDAT2();   /* PB0 Data */
  CLRDAT3();   /* PB1 Instr  */
  CLRDAT();    /* PB2 out */
  CLRCLK();    /* clk */
  
  SETD2L();
  SETD3L();
  SETCL();
  SETDL();

  delay_x1ms(30);
  
  /* VCC */
  ONVCC();
  /* wait for vcc rise */
  /* 30us tiny15 */
  delay50us();
  
  /* for 90s2343 RCEN not programm */
  if(chip1315==2){
    uint8_t i;
    SETDH();
    for( i=0; i<9; ++i ) eight();
  }
  delay50us();
     
  /* VHH */
  ONVHH();
  /* 100ns tiny15 */
  delay50us();
  
  SETDH();

  //delay_x1ms(10);
}

/* ------------------------------------ */

void offt15( void ){
  
  OFFVHH();
  delay50us();

  SETD2H();
  SETD3H();
  SETCH();
  SETDH();
  
  OFFVCC();
}

/* ------------------------------------ */

uint8_t serialtiny15( uint8_t d, uint8_t i ){
  uint8_t j;
  
  CLRDAT2();
  CLRDAT3();
  eight();
  
  for(j=0; j<8; ++j ){
    if(0x80&d) SETDAT2();
    else CLRDAT2();
    if(0x80&i) SETDAT3();
    else CLRDAT3();
    
    d <<= 1;
    i <<= 1;
    if(ISDATP) d |= 0x01;
    
    eight();
  }
  CLRDAT2();
  CLRDAT3();
  eight();
  eight();

  _delay_us(1);
  return d;
}

/* ------------------------------------ */

#endif /* __GR_tiny25 */ 
