/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------*/

#include <stdint.h>
#include <string.h>

#include "util.h"

#if (__AVR_ARCH__ > 1)
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#endif

/*-----------------------------------------------*/

#define __BINBCD16     1
#define _BIN_BCD_SUB   0
#define _BIN_BCD_SH2   1

/*-----------------------------------------------*/

#if _BIN_BCD_SH2==1

#if BINNCDARG==1
void binbcd16( char * tbl, uint16_t ul ){
#else
void binbcd16c( uint16_t ul ) {
#endif

	uint8_t ptl, i;
	uint8_t a89;
/*
	tbl[10] = 0;
	tbl[9] = 0;
	tbl[8] = 0;
	tbl[7] = 0;
	tbl[6] = 0;*/
	tbl[5] = 0;
	
	a89 = 0;
	for(i=0;i<5;++i) {
		a89 = 0;
		ptl = 16;
		while(1) {
			a89 <<= 1;
			if( ul & 0x8000 ) a89 |= 1;
			ul += ul;
			if(a89>=10) {
				a89 -= 10;
				ul |= 0x01;
			}
			if(--ptl == 0 ) break;
		}
    tbl[i] = a89 + '0';
	}
  
}

#endif /* _BIN_BCD_SH2 */

/*-----------------------------------------------*/

#if _BIN_BCD_SUB==1

const uint32_t dectbl[9] PROGMEM = {  1,
                  10,       100,
                1000,     10000,
              100000,   1000000,
            10000000, 100000000 };

						
extern char tbl[];

#if BINNCDARG==1
void binbcd1( char * tbl, uint32_t ul ) {
#else
void binbcd1c( uint32_t ul ) {
#endif

	uint32_t in1, in2;
	unsigned char wsz, a;
	uint16_t *wsk16;

	tbl[10] = 0;
	tbl[9] = 0;
	
	while ( ul > 999999999 ) {
		++tbl[9];
		ul -= 1000000000;
	}
	tbl[9] += '0';
	in1 = ul;
	
	for( wsz=8; wsz&0xFF; --wsz ) {
		wsk16 = (void*)&dectbl[wsz];
		in2 = pgm_read_word(wsk16++);
		in2 += ( (uint32_t)pgm_read_word(wsk16) ) << 16;
		a = 0;
		while (1) {
			in1 -= in2;
			if ( in1 & 0x80000000 )
				break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
	}
	
	tbl[0] = in1+'0';
}

/*-----------------------------------------------*/

/* void (*const binbcd16)( char * tbl, uint32_t ) = binbcd1; */

#if __BINBCD16==1
#if BINNCDARG==1
void binbcd16( char * tbl, uint16_t ul ) {
#else
void binbcd16c( uint16_t ul ) {
#endif
  
	uint16_t in1, in2;
	uint8_t wsz, a;

	tbl[10] = 0;
	tbl[9] = 0;
	tbl[8] = 0;
	tbl[7] = 0;
	tbl[6] = 0;
	tbl[5] = 0;

	in1 = ul;
	a = 0;
	
	for( a=0, in2=10000; in1 & 0x8000; ++a ) in1 -= in2;
	
	for( wsz=4; wsz&0xFF; --wsz ) {
		/* in2 = dectbl[wsz]; */
		in2 = pgm_read_word(&dectbl[wsz]);
		
		while(1){
			in1 -= in2;
			if ( in1 & 0x8000 ) break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
		a = 0;
	}

	tbl[0] = in1+'0';
}
#else

#if BINNCDARG==1
void binbcd16( char * tbl, uint16_t ul ) {
#else
void binbcd16c( uint16_t ul ) {
#endif

  binbcd1( tbl, ul );
}

#endif


#endif /* _BIN_BCD_SUB */

/*-----------------------------------------------*/
/*
char * getnumber32( char * str, uint32_t * liczba ) {
	
	uint32_t l;
	if (str==NULL) return NULL;
	if (*str==0) return NULL;
	while ( (*str<'0') || (*str>'9') ) {
		if (*str == 0) return NULL;
		++str;
	}
	l = 0;
	while ( (*str>='0') && (*str<='9') ) {
		l = (l*10)+(*str-'0');
		++str;
	}
	*liczba = l;
	return str;
}
*/
/*-----------------------------------------------*/
/*
uint8_t readnumb ( char * wsk ) {
	uint8_t ile;
	uint32_t t32;
  
	for ( ile = 0; ile<sizeof(datrstr)/sizeof(datrstr[0] ); ++ile ) {
		wsk = getnumber32( wsk, &t32 );
		if ( !wsk ) break;
		datrstr[ile] = t32;
	}
	
	return ile;
}
*/
/*-----------------------------------------------*/

unsigned char hashexdigit( char tmp2 ) {
	if ( ( tmp2 >= '0' ) && ( tmp2 <= '9' ) ) return tmp2 - ('0'-0);
	if ( ( tmp2 >= 'a' ) && ( tmp2 <= 'f' ) ) return tmp2 - ('a'-10);
	if ( ( tmp2 >= 'A' ) && ( tmp2 <= 'F' ) ) return tmp2 - ('A'-10);
	return 0x80;
}

/*-----------------------------------------------*/

#define CPRV 4

#if CPRV==1
uint8_t cprev( char * dst, char * src , uint8_t siz ){
	char * pti;
	for( pti=dst+siz; ; ) {
		*dst++ = *src--;
		if( ( (uint8_t)(uint16_t)pti == (uint8_t)(uint16_t)dst ) ) break;
	}
	return siz;
}
#endif


/*-----------------------------------------------*/
