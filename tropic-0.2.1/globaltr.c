/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdint.h>

#include "globaltr.h"

volatile uint8_t flag;

struct strprdat prdat;
struct strchip chipinf;

uint16_t rev, id;

uint8_t bufram[BUFRAMSIZ];
uint16_t idxbufin, idxbufou;
uint8_t numdat;

volatile uint16_t voltageznorm;
volatile uint16_t adcres[2];

union union_16_8 wordtosave;
union union_16_8 wordconf;

