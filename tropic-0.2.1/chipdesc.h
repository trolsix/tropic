
#ifndef __FILE_CHIPDESC_H__
#define __FILE_CHIPDESC_H__

#include <stddef.h>
#include "bhead.h"

#if __GR_18f47q10==1
const char str_18f24q10[] PROGMEM = "18f24q10\n";
const char str_18f25q10[] PROGMEM = "18f25q10\n";
const char str_18f26q10[] PROGMEM = "18f26q10\n";
const char str_18f27q10[] PROGMEM = "18f27q10\n";
const char str_18f45q10[] PROGMEM = "18f45q10\n";
const char str_18f46q10[] PROGMEM = "18f46q10\n";
const char str_18f47q10[] PROGMEM = "18f47q10\n";

const uint8_t MEM_18f47q10[] PROGMEM = {
0x00, 0x40, 1,
0x00, 0x80, 1,
0x01, 0x00, 4,
0x02, 0x00, 4,
0x00, 0x80, 1,
0x01, 0x00, 4,
0x02, 0x00, 4
};
#endif

#if __GR_18f4220==1
const char str_18f1220[] PROGMEM = "18f1220\n";
const char str_18f2220[] PROGMEM = "18f2220\n";
const char str_18f4220[] PROGMEM = "18f4220\n";
#endif

#if __GR_12f675==1
const char str_12f629[] PROGMEM = "12f629\n";
const char str_12f675[] PROGMEM = "12f675\n";
#endif

#if __GR_tiny25==1
const char str_90s2343[] PROGMEM = "90s2343\n";
const char str_tiny13[] PROGMEM = "tiny13\n";
const char str_tiny15[] PROGMEM = "tiny15\n";
const char str_tiny25[] PROGMEM = "tiny25\n";
#endif

#if __GR_18f47q10==1
#define _STR_18f24q10_   str_18f24q10,
#define _STR_18f25q10_   str_18f25q10,
#define _STR_18f26q10_   str_18f26q10,
#define _STR_18f27q10_   str_18f27q10,
#define _STR_18f45q10_   str_18f45q10,
#define _STR_18f46q10_   str_18f46q10,
#define _STR_18f47q10_   str_18f47q10,
#else
#define _STR_18f24q10_
#define _STR_18f25q10_
#define _STR_18f26q10_
#define _STR_18f27q10_
#define _STR_18f45q10_
#define _STR_18f46q10_
#define _STR_18f47q10_
#endif

#if __GR_18f4220==1
#define _STR_18f1220_    str_18f1220,
#define _STR_18f2220_    str_18f2220,
#define _STR_18f4220_    str_18f4220,
#else
#define _STR_18f1220_
#define _STR_18f2220_
#define _STR_18f4220_
#endif

#if __GR_12f675==1
#define _STR_12f629_     str_12f629,
#define _STR_12f675_     str_12f675,
#else
#define _STR_12f629_
#define _STR_12f675_
#endif

#if __GR_tiny25==1
#define _STR_90s2343_    str_90s2343,
#define _STR_tiny13_     str_tiny13,
#define _STR_tiny15_     str_tiny15,
#define _STR_tiny25_     str_tiny25,
#else
#define _STR_90s2343_
#define _STR_tiny13_
#define _STR_tiny15_
#define _STR_tiny25_
#endif

const char * const chipname[] PROGMEM = {
  _STR_18f24q10_
  _STR_18f25q10_
  _STR_18f26q10_
  _STR_18f27q10_
  _STR_18f45q10_
  _STR_18f46q10_
  _STR_18f47q10_
  _STR_18f1220_
  _STR_18f2220_
  _STR_18f4220_
  _STR_12f629_
  _STR_12f675_
  _STR_90s2343_
  _STR_tiny13_
  _STR_tiny15_
  _STR_tiny25_
  NULL
};

#if __GR_18f47q10==1
#define _SYGN_18f24q10_   0x71C0,
#define _SYGN_18f25q10_   0x71A0,
#define _SYGN_18f26q10_   0x7180,
#define _SYGN_18f27q10_   0x7100,
#define _SYGN_18f45q10_   0x7140,
#define _SYGN_18f46q10_   0x7120,
#define _SYGN_18f47q10_   0x70E0,
#else
#define _SYGN_18f24q10_
#define _SYGN_18f25q10_
#define _SYGN_18f26q10_
#define _SYGN_18f27q10_
#define _SYGN_18f45q10_
#define _SYGN_18f46q10_
#define _SYGN_18f47q10_
#endif

#if __GR_18f4220==1
#define _SYGN_18f1220_   0x07E0,
#define _SYGN_18f2220_   0x0580, /* mask 0xFFE0 */
#define _SYGN_18f4220_   0x05A0,
#else
#define _SYGN_18f1220_
#define _SYGN_18f2220_
#define _SYGN_18f4220_
#endif

#if __GR_12f675==1
#define _SYGN_12f629_    0x0F80,
#define _SYGN_12f675_    0x0FC0,
#else
#define _SYGN_12f629_
#define _SYGN_12f675_
#endif

#if __GR_tiny25==1
#define _SYGN_90s2343_   0x1E9103,
#define _SYGN_tiny13_    0x1E9007,
#define _SYGN_tiny15_    0x1E9006,
#define _SYGN_tiny25_    0x1E9108,
#else
#define _SYGN_90s2343_
#define _SYGN_tiny13_
#define _SYGN_tiny15_
#define _SYGN_tiny25_
#endif

const uint32_t chipsygnavr[] PROGMEM = {
	_SYGN_90s2343_
	_SYGN_tiny13_
	_SYGN_tiny15_
	_SYGN_tiny25_
	0x00000000
};

const uint16_t chipsygn[] PROGMEM = {
  _SYGN_18f24q10_
  _SYGN_18f25q10_
  _SYGN_18f26q10_
  _SYGN_18f27q10_
  _SYGN_18f45q10_
  _SYGN_18f46q10_
  _SYGN_18f47q10_
  _SYGN_18f1220_
  _SYGN_18f2220_
  _SYGN_18f4220_
  _SYGN_12f629_
  _SYGN_12f675_
  0x0000
};

#if __GR_18f47q10==1
#define _N_18f24q10_ n_18f24q10,
#define _N_18f25q10_ n_18f25q10,
#define _N_18f26q10_ n_18f26q10,
#define _N_18f27q10_ n_18f27q10,
#define _N_18f45q10_ n_18f45q10,
#define _N_18f46q10_ n_18f46q10,
#define _N_18f47q10_ n_18f47q10,
#else
#define _N_18f24q10_
#define _N_18f25q10_
#define _N_18f26q10_
#define _N_18f27q10_
#define _N_18f45q10_
#define _N_18f46q10_
#define _N_18f47q10_
#endif

#if __GR_18f4220==1
#define _N_18f1220_  n_18f1220,
#define _N_18f2220_  n_18f2220,
#define _N_18f4220_  n_18f4220,
#else
#define _N_18f1220_
#define _N_18f2220_
#define _N_18f4220_
#endif

#if __GR_12f675==1
#define _N_12f629_   n_12f629,
#define _N_12f675_   n_12f675,
#else
#define _N_12f629_
#define _N_12f675_
#endif

#if __GR_tiny25==1
#define _N_90s2343_  n_90s2343,
#define _N_tiny13_   n_tiny13,
#define _N_tiny15_   n_tiny15,
#define _N_tiny25_   n_tiny25,
#else
#define _N_90s2343_
#define _N_tiny13_
#define _N_tiny15_
#define _N_tiny25_
#endif

enum numchip {
  _N_18f24q10_
  _N_18f25q10_
  _N_18f26q10_
  _N_18f27q10_
  _N_18f45q10_
  _N_18f46q10_
  _N_18f47q10_
  _N_18f1220_
  _N_18f2220_
  _N_18f4220_
  _N_12f629_
  _N_12f675_
  _N_90s2343_
  _N_tiny13_
  _N_tiny15_
  _N_tiny25_
  _N_END_
};

#endif /* __FILE_CHIPDESC_H__ */
