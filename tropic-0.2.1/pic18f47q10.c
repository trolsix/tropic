/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>

#include <avr/interrupt.h>

#include "globaltr.h"
#include "bhead.h"
#include "pinout.h"
#include "util.h"
#include "uarttx.h"
#include "timecount.h"
#include "readincoming.h"
#include "hardtrans.h"
#include "pic18f47q10.h"

/* ------------------------------------ */

#if __GR_18f47q10==1

/* ------------------------------------ */

#define ADR_FLASH  0x000000UL
#define ADR_USERID 0x200000UL
#define ADR_CONFIG 0x300000UL
#define ADR_EEPROM 0x310000UL
#define ADR_REVID  0x3FFFFCUL

/* ------------------------------------ */
/* need if write and read and increment
static void incadr( uint16_t adrinc ){
  SETDL();
  while(adrinc--) send8bit( 0x80 );
}
*/
/* ------------------------------------ */

static uint16_t read18f47q10one( void ){

  union union_32_8 un32tmp;
  uint16_t res;
  
  SETDL();
  send8bit( 0xFE );
  
  SETDH();
  un32tmp.n8[2] = send8bit( 0x00 ) & 0x7F;
  un32tmp.n8[1] = send8bit( 0x00 );
  un32tmp.n8[0] = send8bit( 0x00 );
  
  res = un32tmp.n32>>1;
  
  return res;
}

/* ------------------------------------ */

static void send18f47q10one( uint32_t d ){

  union union_32_8 un32tmp;
  
  un32tmp.n32 = d<<1;
  
  SETDL();
  send8bit( un32tmp.n8[2] );
  send8bit( un32tmp.n8[1] );
  send8bit( un32tmp.n8[0] );
}

/* ------------------------------------ */

static void setadr( uint32_t adr ){
  SETDL();
  send8bit( 0x80 );
  send18f47q10one( adr );
}

/* ------------------------------------ */

static void readflash18f47q10( uint16_t memsiz ){

  initvaluehex( memsiz );
  
  memsiz >>= 1;
  
  while( memsiz-- ) {
    rssend16univ( read18f47q10one() );
  }
}

/* ------------------------------------ */

static void readdata18f47q10( uint16_t memsiz ){
  
  union union_16_8 eepromdat;
  
  initvaluehex( memsiz );

  memsiz >>= 1;  
  
  while( memsiz-- ) {
    eepromdat.n8[0] = read18f47q10one();
    eepromdat.n8[1] = read18f47q10one();
    rssend16univ( eepromdat.n16 );
  }
}

/* ------------------------------------ */

static void write18f47q10flashword( void ){
  
  uint16_t times, timee;
  union union_32_8 un32tmp;

  setadr( prdat.progstadr32 );
    
  un32tmp.n8[0] = bufdata[0];
  un32tmp.n8[1] = bufdata[1];
  un32tmp.n8[2] = 0;
  
  sendadrinfo();
    
   /* test of protocole dont write*/
  //if( flag & XOFF ) return;
    
  if( prdat.metod == 2 ) un32tmp.n8[1] = 0;
  
  send8bit( 0xC0 ); /* without increment */
  //send8bit( 0xE0 ); /* with increment */
  send18f47q10one( un32tmp.n32 );

  /* t flash 65 us */
  /* t eeprom 11ms */

  if( prdat.metod == 2 ) {
      delay_x1ms( 13 );
  } else {
  
    cli();
    times = 0xFF00;
    times += TCNT2;
    sei();
      
    funincombuf();
    
    while(1){
      cli();
      timee = 0xFF00;
      timee += TCNT2;
      sei();
      /* 100 72 us */
      if( (times - timee) > 100 ) break;
    }
  }
  
}

/* ------------------------------------ */

void read18f47q10fun( void ){
  
//  sendhighadr( ADR_FLASH >> 16 );
  setadr( ADR_FLASH );
  readflash18f47q10( chipinf.sizfbyte );

  sendhighadr( ADR_USERID >> 16 );
  setadr( ADR_USERID );
  readflash18f47q10( 256 );
  
  sendhighadr( ADR_CONFIG >> 16 );
  setadr( ADR_CONFIG );
  readflash18f47q10( 12 );

  sendhighadr( ADR_EEPROM >> 16 );
  setadr( ADR_EEPROM );
  readdata18f47q10( chipinf.sizeebyte );

  hexend();
}
  
/* ------------------------------------ */

void finalize18f47q10fun(void){
  
  if( prdat.metod ) write18f47q10flashword();

}

/* ------------------------------------ */

uint16_t sygn18f47q10( void ){
  setadr( ADR_REVID );
  rev = read18f47q10one();
  id = read18f47q10one();
  return id;
}

/* ------------------------------------ */

void erase18f47q10( void ){
  
  setadr( ADR_CONFIG );
  send8bit( 0x18 );
  delay_x1ms(105); /* 75 ms */
  
  setadr( ADR_EEPROM );
  send8bit( 0x18 );
  delay_x1ms(105); /* 75 ms */
}

/* ------------------------------------ */

void erase18f47q10sector( void ){
  
  uint32_t adrtoerase;
 
  adrtoerase = ADR_FLASH;
  
  while( adrtoerase < chipinf.sizfbyte ){
    setadr( adrtoerase );
    send8bit( 0xF0 );
    delay_x1ms(13); /* 11 ms */
    adrtoerase += 256;
  }
}

/* ------------------------------------ */
/*
struct memsety {
  uint32_t baseadres;
  uint16_t sizbytes;
  uint8_t align;
  uint8_t nrfun;
};

const struct memsety emedat18f47q10[4] PROGMEM = {
  { 0x0000, 0x8000, 2  , 1 },
  { ADR_USERID, 256, 2 , 1 },
  { ADR_CONFIG, 12, 2  , 1 },
  { ADR_EEPROM, 256, 1 , 2 }
};

static uint8_t setadres18f47q10( void ){
  
  if( veryfiadres( ADR_FLASH, chipinf.sizfbyte, 2 ) ) return 1;
}
*/
/* ------------------------------------ */

static uint8_t setadres18f47q10( void ){
  if( veryfiadres( ADR_FLASH, chipinf.sizfbyte, 2 ) ) return 1;
  if( veryfiadres( ADR_USERID, 256, 2 ) ) return 1;
  if( veryfiadres( ADR_CONFIG, 12, 2 ) ) return 1;
  if( veryfiadres( ADR_EEPROM, chipinf.sizeebyte, 1 ) ) return 2;
  return 0;
}

/* ------------------------------------ */

void checkstate18f47q10( void ){

  if( prdat.boundwr == 0 ){
    prdat.metod = setadres18f47q10();
    return;
  }
  
  if( prdat.progadr32 >= prdat.boundwr ){
    if( prdat.metod ) write18f47q10flashword();
    setstrff();
  }
  
  prdat.metod = setadres18f47q10();
}

/* ------------------------------------ */

#endif /* __GR_18f47q10 */


