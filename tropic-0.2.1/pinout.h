
#ifndef __FILE_PINOUT_H__
#define __FILE_PINOUT_H__

#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega328__) || defined(__AVR_ATmega168__)
#include <avr/io.h>
#include <util/delay.h>

#define PWM1        PB1
#define PWM2        PB2

#define PR_DAT      PB0
#define PR_CLK      PD7
#define VCC_EN      PD6
#define VCCCD_EN    PD5
#define VHH_EN      PD4
#define VHHCD_EN    PD2

#define PR_DAT2     PD3
#define PR_DAT3     PB3

#define SETDAT2()   PORTD |= 0x08
#define CLRDAT2()   PORTD &= ~(0x08)
#define SETDAT3()   PORTB |= 0x08
#define CLRDAT3()   PORTB &= ~(0x08)
#define SETD2L()    DDRD |= 0x08
#define SETD2H()    DDRD &= ~(0x08)
#define SETD3L()    DDRB |= 0x08
#define SETD3H()    DDRB &= ~(0x08)

#define SETDAT()    PORTB |= 0x01
#define CLRDAT()    PORTB &= ~(0x01)
#define SETDL()     DDRB |= 0x01
#define SETDH()     DDRB &= ~(0x01)
#define ISDATP      (PINB & 0x01)
#define SETCLK()    PORTD |= 0x80
#define CLRCLK()    PORTD &= ~(0x80)
#define SETCL()     DDRD |= 0x80
#define SETCH()     DDRD &= ~(0x80)
 
#define ONVHH()     PORTD |= 0x10
#define ONVHHCD()   PORTD |= 0x04
#define ONVCC()     PORTD &= ~(0x40)
#define ONVCCCD()   PORTD |= 0x20
 
#define OFFVHH()    PORTD &= ~(0x10)
#define OFFVCC()    PORTD |= 0x40
#define OFFVHHCD()  PORTD &= ~(0x04)
#define OFFVCCCD()  PORTD &= ~(0x20)

#endif

#endif /* __FILE_PINOUT_H__ */

