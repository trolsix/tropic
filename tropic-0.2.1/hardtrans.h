
#ifndef __FILE_HARDTRANS_H__
#define __FILE_HARDTRANS_H__

void send6bit( uint8_t d );
void send4bit( uint8_t d );
uint8_t send8bit( uint8_t d );
void send8bitl( uint8_t d );
uint8_t read8bitl( void );
uint16_t send16bitl( uint16_t d , uint8_t rb );

#endif /* __FILE_HARDTRANS_H__ */
