
#ifndef __FILE_PIC18F1220_H__
#define __FILE_PIC18F1220_H__

#include <stdint.h>

uint16_t sygn18f1220( void );

void read18f1220fun( void );

void erase18f1220noconf( void );
void erase18f1220( void );

void checkstate18f1220( void );
void finalize18f1220(void);

#endif
