
#ifndef __FILE_TINY15_H__
#define __FILE_TINY15_H__

#include <stdint.h>

uint8_t serialtiny15( uint8_t d, uint8_t i );
uint32_t sygntiny15( void );
void entertiny15( void );
void offt15( void );
void set( uint8_t d );
void fusetiny15( void );
void locktiny15( void );

#endif /* TINY15 */
