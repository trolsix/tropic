
#ifndef __FILE_PIC12F675_H__
#define __FILE_PIC12F675_H__

#include <stdint.h>

void read12f675fun( void );
void erase12f675( void );
uint16_t sygn12f675( void );
void checkstate12f675( void );
void finalize12f675fun(void);

#endif
