/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------*/

#include <stdint.h>
#include <string.h>

#include "util.h"

#if (__AVR_ARCH__ > 1)
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#endif

/*-----------------------------------------------*/

#define __BINBCD16 1

/*-----------------------------------------------*/

const uint32_t dectbl[9] PROGMEM = {  1,
                  10,       100,
                1000,     10000,
              100000,   1000000,
            10000000, 100000000 };

						
extern char tbl[];

#if BINNCDARG==1
void binbcd1( char * tbl, uint32_t ul ) {
#else
void binbcd1c( uint32_t ul ) {
#endif

	uint32_t in1, in2;
	unsigned char wsz, a;
	uint16_t *wsk16;

	tbl[10] = 0;
	tbl[9] = 0;
	
	while ( ul > 999999999 ) {
		++tbl[9];
		ul -= 1000000000;
	}
	tbl[9] += '0';
	in1 = ul;
	
	for( wsz=8; wsz&0xFF; --wsz ) {
		wsk16 = (void*)&dectbl[wsz];
		in2 = pgm_read_word(wsk16++);
		in2 += ( (uint32_t)pgm_read_word(wsk16) ) << 16;
		a = 0;
		while (1) {
			in1 -= in2;
			if ( in1 & 0x80000000 )
				break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
	}
	
	tbl[0] = in1+'0';
}

/*-----------------------------------------------*/

/* void (*const binbcd16)( char * tbl, uint32_t ) = binbcd1; */

#if __BINBCD16==1
#if BINNCDARG==1
void binbcd16( char * tbl, uint16_t ul ) {
#else
void binbcd16c( uint16_t ul ) {
#endif
  
	uint16_t in1, in2;
	uint8_t wsz, a;

	tbl[10] = 0;
	tbl[9] = 0;
	tbl[8] = 0;
	tbl[7] = 0;
	tbl[6] = 0;
	tbl[5] = 0;

	in1 = ul;
	a = 0;
	
	for( a=0, in2=10000; in1 & 0x8000; ++a ) in1 -= in2;
	
	for( wsz=4; wsz&0xFF; --wsz ) {
		/* in2 = dectbl[wsz]; */
		in2 = pgm_read_word(&dectbl[wsz]);
		
		while(1){
			in1 -= in2;
			if ( in1 & 0x8000 ) break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
		a = 0;
	}

	tbl[0] = in1+'0';
}
#else

#if BINNCDARG==1
void binbcd16( char * tbl, uint16_t ul ) {
#else
void binbcd16c( uint16_t ul ) {
#endif

  binbcd1( tbl, ul );
}

#endif

/*-----------------------------------------------*/
/*
char * getnumber32( char * str, uint32_t * liczba ) {
	
	uint32_t l;
	if (str==NULL) return NULL;
	if (*str==0) return NULL;
	while ( (*str<'0') || (*str>'9') ) {
		if (*str == 0) return NULL;
		++str;
	}
	l = 0;
	while ( (*str>='0') && (*str<='9') ) {
		l = (l*10)+(*str-'0');
		++str;
	}
	*liczba = l;
	return str;
}
*/
/*-----------------------------------------------*/
/*
uint8_t readnumb ( char * wsk ) {
	uint8_t ile;
	uint32_t t32;
  
	for ( ile = 0; ile<sizeof(datrstr)/sizeof(datrstr[0] ); ++ile ) {
		wsk = getnumber32( wsk, &t32 );
		if ( !wsk ) break;
		datrstr[ile] = t32;
	}
	
	return ile;
}
*/
/*-----------------------------------------------*/
/*      hex to char   */
/*-----------------------------------------------*/
/*
uint8_t hextochar( char * ws ) {

  unsigned char b;

  b = 0;
  
  if ( ( *ws >= '0' ) && ( *ws <= '9' ) ) b = (*ws) - '0';
  else if ( ( *ws >= 'a' ) && ( *ws <= 'f' ) ) b = ((*ws) - 'a')+10;
  else if ( ( *ws >= 'A' ) && ( *ws <= 'F' ) ) b = ((*ws) - 'A')+10;

  b = b << 4;
  ws++; 

  if ( ( *ws >= '0' ) && ( *ws <= '9' ) ) b |= (*ws) - '0';
  else if ( ( *ws >= 'a' ) && ( *ws <= 'f' ) ) b |= ((*ws) - 'a')+10;
  else if ( ( *ws >= 'A' ) && ( *ws <= 'F' ) ) b |= ((*ws) - 'A')+10;

  return b;
}*/
/*-----------------------------------------------*/

unsigned char hashexdigit( char tmp2 ) {
	if ( ( tmp2 >= '0' ) && ( tmp2 <= '9' ) ) return tmp2 - ('0'-0);
	if ( ( tmp2 >= 'a' ) && ( tmp2 <= 'f' ) ) return tmp2 - ('a'-10);
	if ( ( tmp2 >= 'A' ) && ( tmp2 <= 'F' ) ) return tmp2 - ('A'-10);
	return 0x80;
}

/*-----------------------------------------------*/

#define CPRV 4

#if CPRV==1
uint8_t cprev( char * dst, char * src , uint8_t siz ){
	char * pti;
	for( pti=dst+siz; ; ) {
		*dst++ = *src--;
		if( ( (uint8_t)(uint16_t)pti == (uint8_t)(uint16_t)dst ) ) break;
	}
	return siz;
}
#endif
#if CPRV==2
uint8_t cprev( char * dst, char * src , uint8_t siz ){
	
	/* *dst = *src++; */
	src++;
	
	switch (siz) {
		case 66: *dst++ = *--src;
		case 65: *dst++ = *--src;
		case 64: *dst++ = *--src;
		case 63: *dst++ = *--src;
		case 62: *dst++ = *--src;
		case 61: *dst++ = *--src;
		case 60: *dst++ = *--src;
		
		case 59: *dst++ = *--src;
		case 58: *dst++ = *--src;
		case 57: *dst++ = *--src;
		case 56: *dst++ = *--src;
		case 55: *dst++ = *--src;
		case 54: *dst++ = *--src;
		case 53: *dst++ = *--src;
		case 52: *dst++ = *--src;
		case 51: *dst++ = *--src;
		case 50: *dst++ = *--src;

		case 49: *dst++ = *--src;
		case 48: *dst++ = *--src;
		case 47: *dst++ = *--src;
		case 46: *dst++ = *--src;
		case 45: *dst++ = *--src;
		case 44: *dst++ = *--src;
		case 43: *dst++ = *--src;
		case 42: *dst++ = *--src;
		case 41: *dst++ = *--src;
		case 40: *dst++ = *--src;

		case 39: *dst++ = *--src;
		case 38: *dst++ = *--src;
		case 37: *dst++ = *--src;
		case 36: *dst++ = *--src;
		case 35: *dst++ = *--src;
		case 34: *dst++ = *--src;
		case 33: *dst++ = *--src;
		case 32: *dst++ = *--src;
		case 31: *dst++ = *--src;
		case 30: *dst++ = *--src;
		case 29: *dst++ = *--src;
		case 28: *dst++ = *--src;
		case 27: *dst++ = *--src;
		case 26: *dst++ = *--src;
		case 25: *dst++ = *--src;
		case 24: *dst++ = *--src;
		case 23: *dst++ = *--src;
		case 22: *dst++ = *--src;
		case 21: *dst++ = *--src;
		case 20: *dst++ = *--src;
		case 19: *dst++ = *--src;
		case 18: *dst++ = *--src;
		case 17: *dst++ = *--src;
		case 16: *dst++ = *--src;
		case 15: *dst++ = *--src;
		case 14: *dst++ = *--src;
		case 13: *dst++ = *--src;
		case 12: *dst++ = *--src;
		case 11: *dst++ = *--src;
		case 10: *dst++ = *--src;
		case 9: *dst++ = *--src;
		case 8: *dst++ = *--src;
		case 7: *dst++ = *--src;
		case 6: *dst++ = *--src;
		case 5: *dst++ = *--src;
		case 4: *dst++ = *--src;
		case 3: *dst++ = *--src;
		case 2: *dst++ = *--src;
		case 1: *dst++ = *--src;
		//default: break;
	}
	
	return siz;
}
#endif
#if CPRV==3
uint8_t cprev( char * dst, char * src , uint8_t siz ){
	uint8_t i;
	for( i=siz; i; --i ) *dst++ = *src--;
	return siz;
}
#endif

/*-----------------------------------------------*/
