
#ifndef __FILE_UTIL_H__
#define __FILE_UTIL_H__

#define BINNCDARG 1

#if (__AVR_ARCH__ > 1)
#include <avr/pgmspace.h>
#define STR_FLASH(NAME,STRN) const char NAME[] PROGMEM = STRN 
#define STR_EEMEM(NAME,STRN) const char NAME[] EEMEM = STRN 
#else
#define STR_FLASH(NAME,STRN) const char NAME[] = STRN 
#define STR_EEMEM(NAME,STRN) const char NAME[] = STRN 
#endif

#if BINNCDARG==0
#define binbcd1( ARG1, ARG2 ) binbcd1c( ARG2 );
#define binbcd16( ARG1, ARG2 ) binbcd16c( ARG2 );

void binbcd1c(  uint32_t ul );
void binbcd16c( uint16_t ul );
#endif

#if BINNCDARG==1
void binbcd1( char * tbl, uint32_t ul );
void binbcd16( char * tbl, uint16_t ul );
/* void (*const binbcd16)( char * tbl, uint32_t ); */
#endif

char * getnumber32( char * str, uint32_t * liczba );
uint8_t hextochar( char * ws );
unsigned char hashexdigit ( char tmp2 );

uint8_t cprev( char * dst, char * src , uint8_t siz );

#endif /* __FILE_UTIL_H__ */
