/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------*/

#include <stdint.h>

#include "pinout.h"

/* ------------------------------------ */

uint16_t send16bitl( uint16_t d , uint8_t rb ){
  uint8_t i;
  for( i=0; i<16; ++i ){
    if(0x01&d) SETDAT();
    else CLRDAT();
    SETCLK();
    
    if( ( rb ) && ( i==1 ) ) SETDH();
    if( i==15 ) SETDL();
    
    d >>= 1;
    _delay_us(0.3);
    if(ISDATP) d |= 0x8000;
    CLRCLK();
    _delay_us(0.3);
  }
  CLRDAT();
  d >>= 1;
  d &= 0x3FFF;
  _delay_us(2);
  return d;
}

/* ------------------------------------ */

void send6bit( uint8_t d ){
  uint8_t i;
  for( i=0x01; i<0x40; i<<=1 ){
    if(i&d) SETDAT();
    else CLRDAT();
    SETCLK();
    _delay_us(0.3);
    CLRCLK();
    _delay_us(0.3);
  }
  CLRDAT();
  _delay_us(2);
  return;
}

/* ------------------------------------ */

uint8_t send8bit( uint8_t d ){
  uint8_t i;
  for( i=0; i<8; ++i ){
    if(0x80&d) SETDAT();
    else CLRDAT();
    SETCLK();
    _delay_us(0.3);
    d <<= 1;
    if(ISDATP) d |= 0x01;
    CLRCLK();
    _delay_us(0.3);
  }
  CLRDAT();
  _delay_us(1);
  return d;
}

/* ------------------------------------ */

void send4bit( uint8_t d ){
  uint8_t i;
  SETDL();
  for( i=0x01; i<0x10; i<<=1 ){
    if(i&d) SETDAT();
    else CLRDAT();
    SETCLK();
    _delay_us(0.3);
    CLRCLK();
    _delay_us(0.3);
  }
  CLRDAT();
  _delay_us(1);
  return;
}

/* ------------------------------------ */

uint8_t rw8bl( uint8_t d ){
  uint8_t i;
  for( i=0; i<8; ++i ){
    if(0x01&d) SETDAT();
    else CLRDAT();
    SETCLK();
    _delay_us(0.3);
    d >>= 1;
    if(ISDATP) d |= 0x80;
    CLRCLK();
    _delay_us(0.3);
  }
  CLRDAT();
  _delay_us(1);
  return d;
}

/* ------------------------------------ */

void send8bitl( uint8_t d ){
  SETDL();
  rw8bl( d );
}

/* ------------------------------------ */

uint8_t read8bitl( void ){
  SETDH();
  return rw8bl(0);
}

/* ------------------------------------ */

