
#ifndef __FILE_PIC18F47Q10_H__
#define __FILE_PIC18F47q10_H__

#include <stdint.h>

void read18f47q10fun( void );
uint16_t sygn18f47q10( void );
void finalize18f47q10fun(void);

void erase18f47q10sector( void );
void erase18f47q10( void );
	
void checkstate18f47q10( void );

#endif
