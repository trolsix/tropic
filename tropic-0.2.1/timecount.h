
#ifndef __FILE_TIMECOUNT_H__
#define __FILE_TIMECOUNT_H__

#define __TIME_GLOB__         1
#define __TIME_COOUNT__       0
#define __TIME_ADCCOOUNT__    0

void delay_x1ms(uint8_t delay);
uint16_t tim8(void);
uint32_t tim(void);
void delay50us( void );
uint16_t timgetms(void);


extern union uuu {
	uint8_t n8[2];
	uint16_t n16;
} savesp;

#if __TIME_COOUNT__==1
extern uint16_t tirquart;
extern uint16_t tim16;
extern uint32_t tim32a;
extern uint8_t savespl;
extern uint8_t savesph;

uint32_t timget(void);

#if __TIME_ADCCOOUNT__==1
extern uint8_t  tirqadc;
#endif

#else

#undef __TIME_ADCCOOUNT__
#define __TIME_ADCCOOUNT__    0
#endif

extern uint32_t timeoperancion;
extern uint32_t mssec;
extern volatile uint8_t wait8;

#endif

#if __TIME_COOUNT__==1

void showalltime( void );

void ttlist( void );

#define TIME_UART_START()   tirquart = TCNT2
#define TIME_UART_STOP()    tirquart = ((uint16_t)TCNT2-tirquart)<<3
#define TIME_16_START()     tim();
#define TIME_16_STOP()      tim16 = tim();
#define TIME_32_START()     tim32a = timget();
#define TIME_32_STOP()      tim32a = timget() - tim32a;
#else

#define showalltime()

#define TIME_UART_STOP()
#define TIME_UART_START()
#define TIME_16_START()
#define TIME_16_STOP()
#define TIME_32_START()
#define TIME_32_STOP()

#endif /* __FILE_TIMECOUNT_H__ */

