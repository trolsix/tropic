/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------ */

#include <stdint.h>
#include <string.h>

/* ------------------------------------ */

#if (__AVR_ARCH__ > 1)
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#endif

#include "globaltr.h"
#include "bhead.h"
#include "pinout.h"
#include "commandrs.h"
#include "readincoming.h"
#include "util.h"
#include "uarttx.h"
#include "timecount.h"
#include "hardtrans.h"
#include "chipdesc.h"
#include "pic12f675.h"
#include "pic18f1220.h"
#include "pic18f47q10.h"
#include "tiny15.h"

/* ------------------------------------ */

void setflagatomic( uint8_t );
void clrflagatomic( uint8_t );

/* ------------------------------------ */

void (*checkuniv) ( void );

uint32_t timeoperancion;

extern char tbl[11];

void getbinstr( uint32_t data, uint8_t numb );
  
#define FASTSEARCH       0

/* ------------------------------------ */
/* const char ewrite[] EEMEM = "write...\n"; */

#if __USED__EEPROM==1
STR_EEMEM(chnotch,     "choice chip\n");
STR_EEMEM(sbad,        "bad ");
STR_EEMEM(sok,         "OK\n");
STR_EEMEM(ssygn,       "sygnature\n");
STR_EEMEM(strbadchip,  "bad chip\n");
STR_EEMEM(strendok,    "end\n");
STR_EEMEM(sread,       "read\n");
//STR_EEMEM(chclr01,     "check chip is clear...\n");
STR_EEMEM(serase,      "erase\n");
STR_EEMEM(swrite,      "write\n");
//STR_EEMEM(sveryfi,     "veryfi\n");
STR_EEMEM(swtf,        "wtf!\n");
#if __XONXOFF_DIS==1
STR_EEMEM(sxof,        "xoff!\n");
STR_EEMEM(sxon,        "xon!\n");
#endif
STR_EEMEM(sbvoltage,   "bad VHH\n");
#else
#define sendstr_E sendstr_P
STR_FLASH(serase,      "erase\n");
STR_FLASH(swrite,      "write");
//STR_FLASH(sveryfi,     "veryfi\n");
STR_FLASH(swtf,        "wtf!\n");
STR_FLASH(chclr01,     "check\n");
//STR_FLASH(chclr01,     "check chip is clear...\n");
STR_FLASH(sread,       "read\n");
STR_FLASH(chnotch,     "choice chip\n");
STR_FLASH(sbad,        "bad ");
STR_FLASH(sok,         "OK\n");
STR_FLASH(ssygn,       "sygnature\n");
STR_FLASH(strbadchip,  "bad chip\n");
STR_FLASH(strendok  ,  "end\n");
#if __XONXOFF_DIS==1
STR_FLASH(sxof,        "xoff!\n");
STR_FLASH(sxon,        "xon!\n");
#endif
STR_FLASH(sbvoltage,   "bad VHH\n");
#endif

/* ------------------------------------ */

//STR_FLASH( strchclr,     "CHCLR\n");
STR_FLASH( strchip,      "CHIP \n");
STR_FLASH( strchiphelp,  "CHIP name\n");
STR_FLASH( strchiplist,  "CHIP_LIST\n");
STR_FLASH( strcontinue,  "CONT\n");
STR_FLASH( strend,       "END\n");
STR_FLASH( strerase,     "ERASE\n");
//STR_FLASH( strgetu,      "GET_U\n");
STR_FLASH( strhelp,      "HELP\n");
//STR_FLASH( strhexoff,    "HEXOFF\n");
STR_FLASH( strread,      "READ\n");
STR_FLASH( strsygn,      "SYGN\n");
STR_FLASH( strsygnoff,   "SYGN_OFF\n");
STR_FLASH( strsygnon,    "SYGN_ON\n");
STR_FLASH( strwrite,     "WRITE\n");
STR_FLASH( strvccon,     "VCC_ON\n");
//STR_FLASH( strveryfi,    "VERYFI\n");
STR_FLASH( strversion,   "VER\n");
//STR_FLASH( strxonxof,    "XINV\n");

/* ------------------------------------ */
/*
PGM_P const chipname[] PROGMEM = {
  chnr01,
  chnr02,
  NULL
};
*/
/* ------------------------------------ */

void addfun( void(*fun)(void));
void sendvoltage( void );
void showver( void );

/* ------------------------------------ */

void reentry( void );

static void comchip( char * dat );
static void clrstate( void );
static void entry( void );
static void showlist_P( const char **chchp );

/* ------------------------------------ */

static void comchclr( void );
static void comerase( void );
static void comread( void );
static void comsygn( void );
static void comwrite( void );
static void comveryfi( void );

/* ------------------------------------ */

#define IGNORESYGN    0x01
#define BADSYGN       0x02
#define ENTERPRG      0x04
#define SILENT        0x08
#define REE67502      0x10
#define BAD_VOLTAGE   0x20

static uint8_t comflag;

/* ------------------------------------ */

static uint8_t command;
static uint8_t chipnmb;

/* ------------------------------------ */

void comminit(void){
  chipnmb = 0xFF;
  command = COM_NULL;
  comflag = 0;
}

/* ------------------------------------ */

void sendok( void ){
  sendstr_E(sok);
}

/* ------------------------------------ */
/* be carefule , check all latters      */

#if FASTSEARCH==1
static const char tblfl[] PROGMEM = "CCCCEEGHHRSSSWVVX";
#endif

/* ------------------------------------ */

void (*funwsk)(void);
static char * strbuf;

const char * const commname[] PROGMEM = {
//  strchclr,
	strchip,
  strchiphelp,
  strchiplist,
  strcontinue,
  strend,
  strerase,
//	strgetu,
	strhelp,
//  strhexoff,
  strread,
  strsygn,
  strsygnoff,
  strsygnon,
  strwrite,
  strvccon,
//  strveryfi,
  strversion,
//  strxonxof,
  NULL
};

/* ------------------------------------ */
/*
static inline void funchclr( void ){
	command = COM_CHCLR;
	addfun(makecommand);
}
* */
static inline void funhelp( void ){
	showlist_P( (void*) commname );
}
/*
void funhexoff( void ){
	
}*/
static inline void funchip( void ){
  comflag &= ~( ENTERPRG | BADSYGN );
	comchip( strbuf );
}
static inline void funchiplist( void ){
	showlist_P( (void*) chipname );
}
static inline void funcontinue( void ){
  sendstr_E(strcontinue);
}
static inline void funend( void ){
	freestate();
  sendstr_E(strendok);
  comminit();
}
static inline void funerase( void ){
	command = COM_ERASE;
	addfun(makecommand);
}
/*
static inline void fungetu( void ){
  #if __SHOW_ADC==1
	sendvoltage();
  #endif
}*/
static inline void funread( void ){
	command = COM_READ;
	addfun(makecommand);
}
static inline void funsygn( void ){
	command = COM_SYGN;
	addfun(makecommand);
}
static inline void funsygnoff( void ){
	comflag |= IGNORESYGN;
}
static inline void funsygnon( void ){
	comflag &= ~IGNORESYGN;
}
static inline void funwrite( void ){
  comrst();
  freestate();
	command = COM_WRITE;
}
static inline void funvccon( void ){
	ONVCC();
}
static inline void funveryfi( void ){
	command = COM_VERYFI;
}
static inline void funversion( void ){
	showver();
}
static inline void funxonxof( void ){
#if __XONXOFF_DIS==1
  if( flag & XOFF	) {
    clrflagatomic( XOFF );
    sendstr_E(sxon);
  }
  else {
    setflagatomic( XOFF );
    sendstr_E(sxof);
  }
#endif
}
static inline void funempty( void ){
}

/* ------------------------------------ */

uint8_t strinstr_P( const char *str, const char *str_P ){
	char tmp;
	while(1){
		tmp = pgm_read_byte(str_P++);
		if( ( '\n' == tmp || '\r' == tmp ) && ( *str == 0 ) ) return 0;
		if( *str++ != tmp ) return 1;
		if( ( ' ' == tmp ) || ( 0 == tmp )  ) return 0;
	}
}

/* ------------------------------------ */

uint8_t readcommand( char *buf ){

	const char **chchp;
	const char *wsch;
	uint8_t i;
	
	strbuf = buf;
	i = 0;
	
#if FASTSEARCH==1
	{
		char tmp;
		tmp = buf[0];
	
		for( wsch = tblfl; ; ++i ){
			if( tmp == pgm_read_byte(wsch++) ) break;
			if( i >= ( sizeof(tblfl) - 1 ) ) return COM_NULL;
		}
	}
#endif

  chchp = (void*) &commname[i];
  
  for( ; ; ++i, ++chchp ) {
    wsch = (const char*) pgm_read_word(chchp);
    if ( wsch == NULL ) return COM_NULL;
    if( !strinstr_P( buf, wsch ) ) break;
  }
  
	TIME_16_STOP();

  switch( i ) {
    //case 0:   funchclr(); break;
    case 0:   funchip(); break;
    case 1:   funempty(); break;
    case 2:   funchiplist(); break;
    case 3:   funcontinue(); break;
    case 4:   funend(); break;
    case 5:   funerase(); break;
//    case 5:   fungetu(); break;
    case 6:   funhelp(); break;
//  funhexoff,
    case 7:   funread(); break;
    case 8 :  funsygn(); break;
    case 9 :  funsygnoff(); break;
    case 10:  funsygnon(); break;
    case 11:  funwrite(); break;
    case 12:  funvccon(); break;
    //case 15:  funveryfi(); break;
    case 13:  funversion(); break;
    //case 17:  funxonxof(); break;
    default:  break;
  }
	
	return COM_OK;
}

/* ------------------------------------ */

void showlist_P( const char **chchp ){
  
  const char *chch;
  uint8_t i;
 
  for( i=0; ; ++i, ++chchp ){
    chch = (const char*) pgm_read_word(chchp);
    if ( chch == NULL ) break;
	  sendstr_P(chch);
  }
}

/* ------------------------------------ */

void comchip( char * dat ){
  
  const char **chchp;
  const char *chch;
  uint8_t i;
  
  comflag = 0;
  dat += 5;
  chchp = (void*) chipname;
  
  for( i=0; ; ++i, ++chchp ){
    chch = (const char*) pgm_read_word(chchp);
    if ( chch == NULL ) break;
    if( !strinstr_P( dat, chch ) ){
      chipnmb = i;
			sendstr_P(chch);
      return;
    }
  }
  chipnmb = 0xFF;
  sendstr_E(strbadchip);
}

/* ------------------------------------ */
/*
 *
 * 
 * 
 *  
 *   specify function for chip 
 * 
 * 
 * 
 * 
 *
 *
 * ------------------------------------ */

uint8_t id18f25q10( void );

/* ------------------------------------ */

static void comread( void ){
  
  delay_x1ms(150);
  
#if __SHOW_TIME==1
  timeoperancion = timgetms();
#endif

#if __GR_18f47q10==1
  if( chipnmb <= n_18f47q10 ){
    read18f47q10fun();
  } else
#endif
  
#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    read18f1220fun();
  } else
#endif

#if __GR_12f675==1
  if( chipnmb <= n_12f675 ){
    read12f675fun();
  } else
#endif

  {
    return;
  }
  
  freestate();
}

/* ------------------------------------ */

static void comchclr( void ){
/*
  sendstr_E(chclr01);
    
#if __SHOW_TIME==1
  timeoperancion = timgetms();
#endif

  freestate();*/
}

/* ------------------------------------ */

static void comerase( void ){
  
  sendstr_E(serase);
#if __SHOW_TIME==1
  timeoperancion = timgetms();
#endif

#if __GR_18f47q10==1
  if( chipnmb <= n_18f47q10 ){
    //erase18f47q10sector();
    erase18f47q10();
  } else
#endif

#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    //erase18f1220noconf();
    erase18f1220();
  } else
#endif

#if __GR_12f675==1  
  if( chipnmb <= n_12f675 ){
    erase12f675();
  } else
#endif

  {
    return;
  }
  
  freestate();
}

/* ------------------------------------ */

static void comsygn( void ){
  
//  sendstr_E(ssygn);
  
#if __SHOW_TIME==1
  timeoperancion = timgetms();
#endif

#if __GR_18f47q10==1
  if( chipnmb <= n_18f47q10 ){
    rssend16( id );
    rssend16( rev );
  } else
#endif

#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    RSouthex16(sygn18f1220());
  } else
#endif

#if __GR_12f675==1  
  if( chipnmb <= n_12f675 ){
    RSouthex16(sygn12f675());
  } else
#endif  

#if __GR_tiny25==1
  if( chipnmb <= n_90s2343 ){
    set( 2 );
    entertiny15();
    sygntiny15();
    locktiny15();
    offt15();
  } else
  if( chipnmb <= n_tiny13 ){
    set( 1 );
    entertiny15();
    sygntiny15();
    fusetiny15();
    locktiny15();
    offt15();
  } else
  if( chipnmb <= n_tiny15 ){
    set( 0 );
    entertiny15();
    sygntiny15();
    fusetiny15();
    locktiny15();
    offt15();
  } else
#endif

  {
    return;
  }
  
  freestate();
}
 
/* ------------------------------------ */

static void comwrite( void ){
  
  writeunivfun();

}

/* ------------------------------------ */

static void comveryfi( void ){
//  sendstr_E(sveryfi);
}

/* ------------------------------------ */

uint8_t checkcommand( void ){
  if( command != COM_NULL ) return 1;
  return 0;
}

/* ------------------------------------ */

void makecommand( void ){

  if( chipnmb == 0xFF ){
    sendstr_E(chnotch);
    freestate();
    return;
  }
  if(!( comflag & ENTERPRG )) entry();
  if(!( comflag & ENTERPRG )) return;
  numdat = 0;
  switch (command){
    case COM_CHCLR:    comchclr(); break;
    case COM_ERASE:    comerase(); break;
    case COM_READ:     comread(); break;
    case COM_SYGN:     comsygn(); break;
    case COM_WRITE:    comwrite(); break;
    case COM_VERYFI:   comveryfi(); break;
    default:
      //freestate();
      sendstr_E(swtf);
      break;
  }
  idxbufou += prdat.progsiz;
  idxbufou &= BUFRAMMSK;
  prdat.progsiz = 0;
  return;
}

/* ------------------------------------ */

#if __GR_18f47q10==1

uint8_t id18f25q10( void ){
  
  uint16_t tmp16;

  sygn18f47q10();
  tmp16 = pgm_read_word(&chipsygn[chipnmb]);
  
  if( tmp16 != id ){
    return 1;
  }
  
  comflag &= ~BADSYGN;
  
  return 0;
}

#endif

/* ------------------------------------ */

#if __GR_18f4220==1

uint8_t id18f1220( void ){
  
  uint16_t devid, tmp16;

  devid = sygn18f1220();
  
  tmp16 = pgm_read_word(&chipsygn[chipnmb]);
  
  if( ( devid & 0xFFE0) != tmp16 ){
    return 1;
  }
  
  comflag &= ~BADSYGN;
  
  return 0;
}

#endif

/* ------------------------------------ */

#if __GR_12f675==1

uint8_t id12f675( void ){
  
  uint16_t devid, tmp16;
  
  devid = sygn12f675();
  
  tmp16 = pgm_read_word(&chipsygn[chipnmb]);
  
  if( ( devid & 0x3FE0) != tmp16 ){
    return 1;
  }
  
  comflag &= ~BADSYGN;
  
  return 0;
}

#endif

/* ------------------------------------ */

static void clrstate( void ){
  OFFVHH();
  OFFVCC();
  OFFVHHCD();
  OFFVCCCD();
  CLRCLK();
  CLRDAT();
  SETDL();
  SETCL();
}

/* ------------------------------------ */

void finalize( void ){

#if __GR_18f47q10==1
  if( chipnmb <= n_18f47q10 ){
    finalize18f47q10fun();
  } else
#endif

#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    finalize18f1220();
  } else
#endif
  
#if __GR_12f675==1
  if( chipnmb <= n_12f675 ){
    finalize12f675fun();
  } else
#endif

#if __GR_tiny25==1
  if( chipnmb <= n_90s2343 ){
  } else
  if( chipnmb <= n_tiny13 ){
  } else
  if( chipnmb <= n_tiny15 ){
  }
#endif

  RSout('\n');
  command = COM_NULL;
}

/* ------------------------------------ */

STR_FLASH( strdone,      "done\n");

/* ------------------------------------ */

void freestate( void ){
  
  if( comflag & ENTERPRG ) {
    finalize();

#if __SHOW_TIME==1
  timeoperancion = timgetms() - timeoperancion;
#endif
  }

  /* delay for reenew transmision */
  delay_x1ms(200);

#if __SHOW_TIME==1
  RSout('\r');
  binbcd16( tbl, timeoperancion );
  RSouttbl( 5, &tbl[2] );
  RSout('.');
  RSouttbl( 2, tbl );
  RSout(' ');
  RSout('s');
  RSout('e');
  RSout('c');
  RSout('\n');
#else
  sendstr_E(strdone);
#endif

  checkuniv = NULL;
  command = COM_NULL;
  OFFVHH();
  clrstate();
  comflag &= ~( ENTERPRG | BADSYGN | REE67502 );
  SETDH();
  SETCH();
  SETD2H();
  SETD3H();
  memset( &prdat, 0x00, sizeof(prdat) );
  wordconf.n16 = 0xFFFF;
  idxbufou = idxbufin;
}

/* ------------------------------------ */

static void entermode01( void ){
  
  if( comflag & BAD_VOLTAGE ) return;
  
  clrstate();
  delay_x1ms(200); /* vcc should about 0 */
  
  /* VHH */
  ONVHH();
  /* 5us 675 */
  /* 100ns 25q10 */
  /* _delay_us(100); */ 
  delay50us();
  
  /* VCC */
  ONVCC();  
  /* 1ms 25q10 */
  /* 5us 675 */
  
  /* wait for vcc rise */
  delay_x1ms(2);
}

/* ------------------------------------ */

static void entermode02( void ){
  
  if( comflag & BAD_VOLTAGE ) return;
  
  clrstate();
  delay_x1ms(200);

  /* VCC */
  ONVCC();
  
  /* wait for vcc rise */
  /* 100ns 1220 other way test VCC */
  delay_x1ms(2);
  //_delay_us(50);
      
  /* VHH */
  ONVHH();
  /* 2us 1220 */
  delay_x1ms(2);
}

/* ------------------------------------ */
/* add timeout */

#if __TEST_INCOMING==1
void waitforvoltage( uint16_t voltage ){
  return;
}
#else
void waitforvoltage( uint16_t voltage ){
  
  uint16_t tmpadc;
  uint8_t timewaitvol;
  voltageznorm = voltage;
  
  for( timewaitvol=100 ; --timewaitvol ; ) {
    delay_x1ms(10);
    cli();
    tmpadc = adcres[adcvhh];
    sei();
    if( tmpadc < (voltageznorm + 2 ) ){
      if( tmpadc > ( voltageznorm - 10 ) ){
        comflag &= ~BAD_VOLTAGE;
        return;
      }
    }
  }

  comflag |= BAD_VOLTAGE;
}
#endif

/* ------------------------------------ */

void reentry( void ){

#if __GR_18f47q10==1
  if( chipnmb <= n_18f47q10 ){
    waitforvoltage( 1164 );
    entermode01();
  } else
#endif

#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    waitforvoltage( 1500 );
    entermode02();
  } else
#endif

#if __GR_12f675==1
  if( chipnmb <= n_12f675 ) {
    waitforvoltage( 1500 );
    if( comflag & REE67502 ) entermode01();
    else entermode02();
  }else
#endif

#if __GR_tiny25==1  
  if( chipnmb <= n_tiny25 ) {
    waitforvoltage( 1520 );
  } else
#endif

  {
    return;
  }
  
  if( comflag & BAD_VOLTAGE ) clrstate();
  
}

/* ------------------------------------ */

static void entry( void ){

#if __SHOW_TIME==1
  timeoperancion = timgetms();
#endif

  reentry();

  comflag |= BADSYGN;
  
#if __GR_18f47q10==1  
  if( chipnmb <= n_18f47q10 ){
    uint8_t ptt, * pttf;
    union union_32_8 tmpu32u8;
      
    id18f25q10();
    checkuniv = checkstate18f47q10;
    
    ptt = chipnmb - n_18f24q10;
    ptt = ptt+ptt+ptt;
    
    pttf = (uint8_t*)MEM_18f47q10;
    pttf += ptt;
    
    tmpu32u8.n8[0] = 0;
    tmpu32u8.n8[3] = 0;
    tmpu32u8.n8[2] = pgm_read_byte(pttf++);
    tmpu32u8.n8[1] = pgm_read_byte(pttf++);
    chipinf.sizfbyte = tmpu32u8.n32;
    
    tmpu32u8.n8[1] = pgm_read_byte(pttf++);
    chipinf.sizeebyte = tmpu32u8.n32;
  } else
#endif

#if __GR_18f4220==1
  if( chipnmb <= n_18f4220 ){
    checkuniv = checkstate18f1220;
    chipinf.sizfbyte = 0x1000; /* now only 1220 */
    id18f1220();
  } else
#endif

#if __GR_12f675==1
  if( chipnmb <= n_12f675 ){
    checkuniv = checkstate12f675;
    id12f675();
    if( comflag & BADSYGN ){
      comflag |= REE67502;
      reentry();
      id12f675();
    }
  } else
#endif

#if __GR_tiny25==1
  if( chipnmb <= n_tiny25 ){
    comflag &= ~BADSYGN;
    sendok();
  } else
#endif

  {
    return;
  }
  
  if( comflag & BAD_VOLTAGE ){
    if( command & COM_READ ) RSout( EOT );
    delay_x1ms(200);
    sendstr_E(sbvoltage);
    return;
  }
  
  
  comflag |= ENTERPRG;
  /* if read optin send en of transmision */
  
  if( comflag & BADSYGN ){
    if( comflag & IGNORESYGN ){
    } else {
      if( command & COM_READ ) RSout( EOT );
      delay_x1ms(200);
      sendstr_E(sbad);
      comflag &= ~ENTERPRG;
      sendstr_E(ssygn);
      freestate();
    }
  }  

  return;
}

/* ------------------------------------ */

uint8_t w;

void writeunivfun( void ){
  
  if( w&0x01)RSout(' ');
  else RSout('.');
  ++w;
  if( w == 33 ) RSout('\r');
  if( w >= 64 ) { RSout('\r'); w = 0; }
  
  if( prdat.boundwr == 0 ){
    
#if __SHOW_TIME==1
    timeoperancion = timgetms();
#endif
    setstrff();
  }
  
  /* test rising adres */
  if( prdat.progadr32 < prdat.progstadr32 ) return;
  
  for( ; prdat.progsiz; --prdat.progsiz ){
    
    if( checkuniv ) checkuniv();
    pbufd =  prdat.progadr32 - prdat.progstadr32;
      
    if( pbufd >= sizeof(bufdata) ) return; /* warn */
    
    bufdata[pbufd] = bufram[idxbufou++];
    idxbufou &= BUFRAMMSK;
    ++prdat.progadr32;
  }

}

/* ------------------------------------ */

uint8_t bufdata[8];
uint8_t pbufd;

void setstrff( void ){
  memset( bufdata, 0xFF, 8 );
  pbufd = 0;
}

/* ------------------------------------ */

uint8_t veryfiadres( uint32_t adres, uint32_t siz, uint8_t bound ){

  if( prdat.progadr32 < adres ) return 0;
  if( prdat.progadr32 >= ( adres + siz ) ) return 0;
  
  if( prdat.boundwr < adres ) prdat.boundwr = adres;
  while( prdat.boundwr <= prdat.progadr32 ) prdat.boundwr += bound;
  prdat.progstadr32 = prdat.boundwr - bound;

  return 1;
}

/* ------------------------------------ */

#define __SEND_AADR_INFO 0

void sendadrinfo( void ) {
#if __SEND_AADR_INFO==1
  rssend16( prdat.progstadr32>>16 );
  rssend16( prdat.progstadr32 );

  rssend8( bufdata[0] );
  rssend8( bufdata[1] );
  rssend8( bufdata[2] );
  rssend8( bufdata[3] );
  rssend8( bufdata[4] );
  rssend8( bufdata[5] );
  rssend8( bufdata[6] );
  rssend8( bufdata[7] );
  numdat = 0; /* test */

  RSout('\n');
#else

#endif  
}


/* ------------------------------------ */
/*
 * 18f25q10
 * Vpp 7.9-9.0
 * mclr rise 1us
 * clk max 5MHz 100ns state
 * erase 75ms
 * eeprom 11ms
 * flash 65us
 * */
/* ------------------------------------ */
/*
 * 18f1220
 * Vpp 9.0-13.25
 * mclr rise 1us
 * clk max 10MHz 5V, 1MHz 2V
 * erase 10ms
 * eeprom 10ms
 * flash 1ms
 * */
/* ------------------------------------ */

