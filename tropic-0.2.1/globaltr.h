
#ifndef __FILE_GLOBALTR_H__
#define __FILE_GLOBALTR_H__

#include <stdint.h>
#include "bhead.h"

extern uint8_t bufdata[8];
extern uint8_t pbufd;

void initvaluehex( uint16_t memsiz );

void writeunivfun( void );
void setstrff( void );
void getbinstr( uint32_t data, uint8_t numb );
void sendadrinfo( void );
uint8_t veryfiadres( uint32_t adres, uint32_t siz, uint8_t bound );

#define ENDWAIT    0x01
#define ISDATA     0x02
//#define OFFWRITE   0x04
#define HEXACT     0x08
#define BUFINOV    0x10
#define XONXOFF    0x20
#define NSENDHEX   0x40
#define XOFF       0x80

extern volatile uint8_t flag;

#define EOT        0x04

struct strprdat {
  uint16_t progstadr16;
  uint32_t progstadr32;
  uint16_t progadr16;
  uint32_t progadr32;
  uint16_t progsiz;
  uint32_t boundwr;
  uint8_t  flag;
  uint8_t  metod;
};

struct strchip {
  uint32_t sizfbyte;
	uint16_t sizeebyte;
};

extern struct strprdat prdat;
extern struct strchip chipinf;
extern uint16_t rev, id;
extern uint8_t numdat;

#define BUFRAMSIZ 256
#define BUFRAMMSK (BUFRAMSIZ-1)
#define BUFRAMFIL (BUFRAMSIZ-32)

extern uint8_t bufram[BUFRAMSIZ];
extern uint16_t idxbufin, idxbufou;

enum { adcvcc, adcvhh };
extern volatile uint16_t voltageznorm;
extern volatile uint16_t adcres[2];

union union_16_8 {
	uint8_t n8[2];
	uint16_t n16;
};

union union_32_8 {
	uint8_t n8[4];
	uint32_t n32;
};

extern union union_16_8 wordtosave;
extern union union_16_8 wordconf;

#endif /* __FILE_GLOBALTR_H__ */

