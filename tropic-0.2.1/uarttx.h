
#ifndef __FILE_UARTTX_H__
#define __FILE_UARTTX_H__

#define UARTTXBUF      0

void RSout( uint8_t d );
void RSouttbl( unsigned char siz, const char *tblf );
void RSoutstr( const char *tblf );
void RSouthex16( uint16_t d );
void RSouthex8( uint8_t d );
void uart_init( void );
void sendstr_P( const char * pP );
void sendstr_E( const char * pP );

void rssend16univ( uint16_t d );
void hexend( void );
void sendhighadr( uint16_t d );

void rssend8( uint8_t d );
void rssend16( uint16_t d );

#if UARTTXBUF==1
/* this must be power of 2 and >= 4 */
#define UART1BUF 8
#endif

#endif /* __FILE_UARTTX_H__ */
