/*
 * Copyright (C) 2021 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------*/

#include <stdint.h>
#include <string.h>

#include "globaltr.h"
#include "bhead.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

/*-----------------------------------------------*/

#define __BAUD__ 7

#if __BAUD__==1
#define BAUD (2400UL * 8)
#elif __BAUD__==2
#define BAUD (4800UL * 8)
#elif __BAUD__==3
#define BAUD (9600UL * 8)
#elif __BAUD__==4
#define BAUD (19200UL * 8)
#elif __BAUD__==5
#define BAUD (38400UL * 8)
#elif __BAUD__==6
#define BAUD (57600UL * 8)
#elif __BAUD__==7
#define BAUD (115200UL * 8)
#else
#error BAUD not set property in uarttx.c
#endif

/*-----------------------------------------------*/

#include "uarttx.h"

/*-----------------------------------------------*/
/*
xon xoff pause 19 start 17
*/

#if UARTTXBUF==1
#define UART1MSK (UART1BUF-1)
#define UART1FIL (UART1BUF-2)
static volatile uint8_t wsrsbufin;
static volatile uint8_t wsrsbufou;
static char rsbuf[UART1BUF];
#endif

/*-----------------------------------------------*/

uint8_t CRC;

/*-----------------------------------------------*/

void uart_init( void ) {
#ifdef UDR
	UBRRL = (F_CPU / BAUD) - 1;
	UBRRH = ( (F_CPU / BAUD) - 1 ) >> 8;
	
	UCSRA = 1<<U2X;
	UCSRC = (3<<UCSZ0)|(1<<URSEL);
	UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
	UDR;
#endif
#ifdef UDR0
	UBRR0 = (uint16_t) (F_CPU / BAUD) - 1;
	UCSR0A = 1<<U2X0;
	UCSR0C = 3<<UCSZ00;
	UCSR0B = (1<<TXEN0)|(1<<RXEN0)|(1<<RXCIE0);
	UDR0;	
#endif
#if UARTTXBUF==1
	wsrsbufin = 0;
	wsrsbufou = 0;
#endif
}

/*-----------------------------------------------*/

void RSouttbl( unsigned char siz, const char *tblf ) {
	while(siz) { RSout( tblf[--siz] ); if(siz==0) return; }
}

/*-----------------------------------------------*/

void RSoutstr( const char *tblf ) {
  while(*tblf) { RSout(*tblf); ++tblf; }
}

/*-----------------------------------------------*/

void RSouthex16( uint16_t d ) {
  RSout('0');
  RSout('x');
  RSouthex8( d>>8 );
  RSouthex8( d );
}

/*-----------------------------------------------*/

void RSouthex8( uint8_t d ) {
  char ch;
  ch = (d>>4)&0x0F;
  if( ch > 9 ) RSout('A'-10+ch);
  else RSout('0'+ch);
  ch = d&0x0F;
  if( ch > 9 ) RSout('A'-10+ch);
  else RSout('0'+ch);
  CRC -= d;
}

/*-----------------------------------------------*/

#if UARTTXBUF==1
ISR(USART_UDRE_vect){
	
	uint8_t tmp;
	
	/* bufor empty disble irq */
	tmp = wsrsbufou;
	if( wsrsbufin == tmp ){
    _DIS_UART_EMPTY();
		return;
	}
	
	if( 0x80 & rsbuf[tmp] ) _REG_UART0 = '?';
	else _REG_UART0 = rsbuf[tmp];
	wsrsbufou = (1+tmp) & UART1MSK;
}
#endif

/*-----------------------------------------------*/
/* 1-126*/

void RSout( uint8_t d ) {

	#if UARTTXBUF==1

	while(1){
		uint8_t howfill;
		howfill = (wsrsbufin-wsrsbufou) & UART1MSK;
		if ( howfill < UART1FIL ) break;
	}
	rsbuf[wsrsbufin] = d;	
	wsrsbufin = (1+wsrsbufin) & UART1MSK;
	cli();
  _EN_UART_EMPTY();
	sei();
	#else
	while ( 0 == _UART0_ISEMPTY );
  _REG_UART0 = d;
	#endif
}

/*-----------------------------------------------*/

void sendstr_P( const char * pP ){
	while(1) {
		char a;
		a = pgm_read_byte(pP++);
		if ( a < 1 ) break;
		RSout( a );
	}	
}

/*-----------------------------------------------*/

#if __USED__EEPROM==1

void sendstr_E( const char * pP ){
	while(1) {
		char a;
		a = eeprom_read_byte((const uint8_t *)pP++);
		if ( a < 1 ) break;
		RSout( a );
	}	
}

#endif

/*-----------------------------------------------*/

void rssend8( uint8_t d ){
  RSouthex8( d );
  if( ++numdat >= 16 ) {
    RSout('\n');
    numdat = 0;
  } else RSout(' ');
}

/*-----------------------------------------------*/

void rssend16( uint16_t d ){
  RSouthex8( d>>8 );
  RSouthex8( d );
  if( ++numdat >= 16 ){
    RSout('\n');
    numdat = 0;
  } else RSout(' ');
}

/*-----------------------------------------------*/

const char strendlhex[] PROGMEM = ":00000001FF\n";

/*-----------------------------------------------*/

void sendhighadr( uint16_t d ){
  CRC = 0;
  RSout(':');
  RSouthex8( 0x02 );
  RSouthex8( 0x00 );
  RSouthex8( 0x00 );
  RSouthex8( 0x04 );
  RSouthex8( d>>8 );
  RSouthex8( d );
  RSouthex8( CRC );
  RSout('\n'); 
}

/*-----------------------------------------------*/

void hexend( void ){
  //if(!( flag & NSENDHEX )){
    sendstr_P(strendlhex);
    RSout( EOT );
  //}
}

/*-----------------------------------------------*/

void rssend16univ( uint16_t d ){
  uint16_t progsiztmp;
  progsiztmp = prdat.progsiz;
  //if(!( flag & NSENDHEX )){
    if( numdat == 0 ){
      CRC = 0;
      RSout(':');
      if( progsiztmp > 16 ){
        RSouthex8( 16 );
      }
      else {
        RSouthex8( progsiztmp );
      }
      RSouthex8( prdat.progstadr16>>8 );
      RSouthex8( prdat.progstadr16 );
      RSouthex8( 0 );
    }
    progsiztmp -= 2;
    prdat.progstadr16 += 2; /* adr is 2byte */
    RSouthex8( d );
    RSouthex8( d>>8 );
    ++numdat;
    if( ( numdat >= 8 ) || ( 0 == progsiztmp) ){
      RSouthex8( CRC );
      RSout('\n');
      numdat = 0;
    }
  //}
  //else rssend16( d );
  prdat.progsiz = progsiztmp;
}

/*-----------------------------------------------*/

